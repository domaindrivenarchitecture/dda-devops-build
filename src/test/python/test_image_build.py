import os
from pybuilder.core import Project
from src.main.python.ddadevops import DevopsImageBuild
from .domain.helper import devops_config


def test_devops_docker_build(tmp_path):
    str_tmp_path = str(tmp_path)
    project = Project(str_tmp_path, name="name")

    os.environ["IMAGE_DOCKERHUB_USER"] = "user"
    os.environ["IMAGE_DOCKERHUB_PASSWORD"] = "password"

    image_build = DevopsImageBuild(
        project,
        devops_config(
            {
                "project_root_path": str_tmp_path,
                "build_types": ["IMAGE"],
                "mixin_types": [],
            }
        ),
    )
    assert image_build
