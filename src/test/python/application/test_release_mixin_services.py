import pytest
from pathlib import Path
from src.main.python.ddadevops.domain import (
    ReleaseType,
    MixinType,
)
from src.test.python.domain.helper import (
    BuildFileRepositoryMock,
    GitApiMock,
    ArtifactDeploymentApiMock,
    build_devops,
)
from src.main.python.ddadevops.application import ReleaseService


def test_should_update_release_type():
    sut = ReleaseService(
        GitApiMock(), ArtifactDeploymentApiMock(), BuildFileRepositoryMock("build.py")
    )
    devops = build_devops({})
    release = devops.mixins[MixinType.RELEASE]
    sut.update_release_type(release, "MAJOR")
    assert ReleaseType.MAJOR == release.release_type

    with pytest.raises(Exception):
        sut.update_release_type(release, "NOT_EXISTING")


def test_should_publish_artifacts():
    mock = ArtifactDeploymentApiMock(release='{"id": 2345}')
    sut = ReleaseService(GitApiMock(), mock, BuildFileRepositoryMock())
    devops = build_devops(
        {
            "release_artifacts": ["target/art"],
            "release_artifact_server_url": "http://repo.test/",
            "release_organisation": "orga",
            "release_repository_name": "repo",
        }
    )
    release = devops.mixins[MixinType.RELEASE]
    sut.publish_artifacts(release)
    assert "http://repo.test/api/v1/repos/orga/repo/releases/2345/assets" == mock.add_asset_to_release_api_endpoint

def test_should_throw_exception_if_there_was_an_error_in_publish_artifacts():
    devops = build_devops(
        {
            "release_artifacts": ["target/art"],
            "release_artifact_server_url": "http://repo.test/",
            "release_organisation": "orga",
            "release_repository_name": "repo",
        }
    )
    release = devops.mixins[MixinType.RELEASE]
    
    with pytest.raises(Exception):
        mock = ArtifactDeploymentApiMock(release='')
        sut = ReleaseService(GitApiMock(), mock, BuildFileRepositoryMock())
        sut.publish_artifacts(release)

    with pytest.raises(Exception):
        mock = ArtifactDeploymentApiMock(release='{"message": "there was an error", "url":"some-url"}')
        sut = ReleaseService(GitApiMock(), mock, BuildFileRepositoryMock())
        sut.publish_artifacts(release)
