from pathlib import Path
from src.main.python.ddadevops.infrastructure import ExecutionApi


def copy_resource(source: Path, target: Path):
    api = ExecutionApi()
    res_source = Path('src/test/resources/').joinpath(source)
    api.execute(f"cp {str(res_source)} {str(target)}")
