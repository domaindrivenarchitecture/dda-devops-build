import pytest as pt
import os
from pathlib import Path
from pybuilder.core import Project

from src.main.python.ddadevops.release_mixin import ReleaseMixin
from src.main.python.ddadevops.domain import Devops, Release
from src.main.python.ddadevops.application import ReleaseService
from src.main.python.ddadevops.infrastructure import BuildFileRepository
from .domain.helper import devops_config, GitApiMock, ArtifactDeploymentApiMock
from .resource_helper import copy_resource


def test_release_mixin(tmp_path):
    str_tmp_path = str(tmp_path)
    copy_resource(Path("package.json"), tmp_path)
    project = Project(str_tmp_path, name="name")

    os.environ["RELEASE_ARTIFACT_TOKEN"] = "ratoken"

    sut = ReleaseMixin(
        project,
        devops_config(
            {
                "project_root_path": str_tmp_path,
                "mixin_types": ["RELEASE"],
                "build_types": [],
                "module": "release-test",
            }
        ),
    )

    sut.initialize_build_dir()
    assert sut.build_path() == f"{str_tmp_path}/target/name/release-test"

def test_release_mixin_different_version_suffixes(tmp_path):
    str_tmp_path = str(tmp_path)
    copy_resource(Path("config.py"), tmp_path)
    copy_resource(Path("config.gradle"), tmp_path)

    project = Project(str_tmp_path, name="name")

    os.environ["RELEASE_ARTIFACT_TOKEN"] = "ratoken"

    sut = ReleaseMixin(
        project,
        devops_config(
            {
                "project_root_path": str_tmp_path,
                "mixin_types": ["RELEASE"],
                "build_types": [],
                "module": "release-test",
                "release_current_branch": "main",
                "release_main_branch": "main",
                "release_primary_build_file": "config.py",
                "release_secondary_build_files": ["config.gradle"],
            }
        ),
    )
    sut.release_service = ReleaseService(GitApiMock(), ArtifactDeploymentApiMock(), BuildFileRepository(project.basedir))

    sut.initialize_build_dir()
    sut.update_release_type("PATCH")
    sut.prepare_release()
    sut.tag_bump_and_push_release()
    
    assert sut.release_service.build_file_repository.get(Path("config.py")).get_version().to_string() == "3.1.5-dev"
    assert sut.release_service.build_file_repository.get(Path("config.gradle")).get_version().to_string() == "3.1.5-SNAPSHOT"