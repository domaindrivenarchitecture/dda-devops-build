from pybuilder.core import Project
from pathlib import Path
from src.main.python.ddadevops.domain import (
    BuildType,
    Digitalocean,
)
from .helper import devops_config


def test_digitalocean_creation():
    sut = Digitalocean(
        {
            "module": "module",
            "stage": "test",
            "do_api_key": "api_key",
            "do_spaces_access_id": "spaces_id",
            "do_spaces_secret_key": "spaces_secret",
        }
    )
    assert sut is not None
    assert sut.is_valid()

    sut = Digitalocean(
        {
            "module": "module",
            "stage": "test",
            "do_api_key": "api_key",
            "do_spaces_access_id": "spaces_id",
            "do_spaces_secret_key": "spaces_secret",
            "do_as_backend": True,
            "do_account_name": "account_name",
            "do_endpoint": "endpoint",
            "do_bucket": "bucket",
            "do_bucket_key": "bucket_key",
            "do_region": "region",
        }
    )
    assert sut is not None
    assert sut.is_valid()


def test_should_calculate_backend_config():
    sut = Digitalocean(devops_config({}))
    assert {
        "access_key": "spaces_id",
        "secret_key": "spaces_secret",
        "endpoint": "endpoint",
        "bucket": "bucket",
        "key": "test/module",
        "region": "region",
    } == sut.backend_config()


def test_should_calculate_project_vars():
    sut = Digitalocean(
        devops_config(
            {
                "do_as_backend": False,
            }
        )
    )
    assert {
        "do_api_key": "api_key",
        "do_spaces_access_id": "spaces_id",
        "do_spaces_secret_key": "spaces_secret",
    } == sut.project_vars()

    sut = Digitalocean(
        devops_config(
            {
                "do_as_backend": True,
            }
        )
    )
    assert {
        "do_api_key": "api_key",
        "do_spaces_access_id": "spaces_id",
        "do_spaces_secret_key": "spaces_secret",
        "region": "region",
        "account_name": "test",
        "endpoint": "endpoint",
        "bucket": "bucket",
        "key": "test/module",
    } == sut.project_vars()
