import pytest
from pathlib import Path
from src.main.python.ddadevops.domain import (
    CredentialMapping,
    Credentials,
    GopassType,
    MixinType,
)
from .helper import build_devops


def test_should_create_mapping():
    sut = CredentialMapping(
        {
            "gopass_path": "server/meissa/grafana-cloud",
            "gopass_field": "grafana-cloud-user",
        }
    )
    assert "grafana_cloud_user" == sut.name_for_input()
    assert "GRAFANA_CLOUD_USER" == sut.name_for_environment()
    assert GopassType.FIELD == sut.gopass_type()

    sut = CredentialMapping(
        {
            "gopass_path": "server/meissa/grafana-cloud",
            "name": "grafana_cloud_password",
        }
    )
    assert "grafana_cloud_password" == sut.name_for_input()
    assert "GRAFANA_CLOUD_PASSWORD" == sut.name_for_environment()
    assert GopassType.PASSWORD == sut.gopass_type()

    sut = CredentialMapping(
        {
            "gopass_path": "server/meissa/grafana-cloud",
            "gopass_field": "grafana-cloud-user",
            "name": "gfc_user",
        }
    )
    assert "gfc_user" == sut.name_for_input()
    assert "GFC_USER" == sut.name_for_environment()
    assert GopassType.FIELD == sut.gopass_type()


def test_should_validate_CredentialMapping():
    sut = CredentialMapping(
        {
            "gopass_path": "server/meissa/grafana-cloud",
            "gopass_field": "grafana-cloud-user",
        }
    )
    assert sut.is_valid()

    sut = CredentialMapping(
        {
            "gopass_path": "server/meissa/grafana-cloud",
            "name": "grafana_cloud_user",
        }
    )
    assert sut.is_valid()

    sut = CredentialMapping(
        {
            "gopass_path": "server/meissa/grafana-cloud",
        }
    )
    assert not sut.is_valid()


def test_should_create_credentials():
    sut = Credentials(
        {
            "credentials_mapping": [
                {
                    "gopass_path": "server/meissa/grafana-cloud",
                    "gopass_field": "grafana-cloud-user",
                },
                {
                    "gopass_path": "server/meissa/grafana-cloud",
                    "name": "grafana_cloud_password",
                },
            ],
        }
    )
    assert sut
    assert 2 == len(sut.mappings)

    sut = Credentials(
        {},
        default_mappings=[
            {
                "gopass_path": "server/meissa/grafana-cloud",
                "gopass_field": "grafana-cloud-user",
            },
            {
                "gopass_path": "server/meissa/grafana-cloud",
                "name": "grafana_cloud_password",
            },
        ],
    )
    assert sut
    assert 2 == len(sut.mappings)

    sut = Credentials(
        {
            "credentials_mapping": [
                {
                    "gopass_path": "dome/path",
                    "gopass_field": "some-field",
                },
                {
                    "gopass_path": "another_path",
                    "name": "grafana_cloud_password",
                },
            ],
        },
        default_mappings=[
            {
                "gopass_path": "server/meissa/grafana-cloud",
                "gopass_field": "grafana-cloud-user",
            },
            {
                "gopass_path": "server/meissa/grafana-cloud",
                "name": "grafana_cloud_password",
            },
        ],
    )
    assert sut
    assert 3 == len(sut.mappings)
    assert sut.mappings["grafana_cloud_password"].gopass_path == "another_path"


def test_should_validate_credentials():
    sut = Credentials(
        {
            "credentials_mapping": [
                {
                    "gopass_path": "server/meissa/grafana-cloud",
                    "gopass_field": "grafana-cloud-user",
                },
                {
                    "gopass_path": "server/meissa/grafana-cloud",
                    "name": "grafana_cloud_password",
                },
            ],
        }
    )
    assert sut.is_valid()

    sut = Credentials(
        {
            "credentials_mapping": [
                {
                    "gopass_path": "server/meissa/grafana-cloud",
                    "gopass_field": "grafana-cloud-user",
                },
                {"gopass_path": "server/meissa/grafana-cloud"},
            ],
        }
    )
    assert not sut.is_valid()
