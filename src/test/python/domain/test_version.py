from pybuilder.core import Project
from pathlib import Path
from src.main.python.ddadevops.domain import (
    Version,
    ReleaseType,
    Image,
)
from .helper import build_devops, devops_config


def test_version_creation():
    sut = Version.from_str("1.2.3", "SNAPSHOT")
    assert sut.to_string() == "1.2.3"
    assert sut.version_list == [1, 2, 3]
    assert sut.is_snapshot() == False

    sut = Version.from_str("1.2.3-SNAPSHOT", "SNAPSHOT")
    assert sut.to_string() == "1.2.3-SNAPSHOT"
    assert sut.version_list == [1, 2, 3]
    assert sut.is_snapshot() == True


def test_should_validate_version_list():
    sut = Version(None, "SNAPSHOT")
    assert not sut.is_valid()

    sut = Version([], "SNAPSHOT")
    assert not sut.is_valid()

    sut = Version([1, 2], "SNAPSHOT")
    assert not sut.is_valid()

    sut = Version([1, 2, 3], "SNAPSHOT")
    assert sut.is_valid()


def test_should_validate_parsing():
    sut = Version.from_str("1.2", "SNAPSHOT")
    assert not sut.is_valid()

    sut = Version.from_str("1.2.3", "SNAPSHOT")
    sut.version_list = [2, 2, 2]
    assert not sut.is_valid()

    sut = Version.from_str("1.2.3", "SNAPSHOT")
    assert sut.is_valid()

    sut = Version.from_str("1.2.3-SNAPSHOT", "SNAPSHOT")
    assert sut.is_valid()

    sut = Version.from_str("1.2.3-dev", "dev")
    assert sut.is_valid()


def test_should_create_patch():
    version = Version.from_str("1.2.3-SNAPSHOT", "SNAPSHOT")
    sut = version.create_patch()
    assert sut.to_string() == "1.2.3"
    assert version.to_string() == "1.2.3-SNAPSHOT"

    version = Version.from_str("1.2.3", "SNAPSHOT")
    sut = version.create_patch()
    assert sut.to_string() == "1.2.4"
    assert version.to_string() == "1.2.3"


def test_should_create_minor():
    version = Version.from_str("1.2.3-SNAPSHOT", "SNAPSHOT")
    sut = version.create_minor()
    assert sut.to_string() == "1.3.0"

    version = Version.from_str("1.2.3", "SNAPSHOT")
    sut = version.create_minor()
    assert sut.to_string() == "1.3.0"

    version = Version.from_str("1.3.0-SNAPSHOT", "SNAPSHOT")
    sut = version.create_minor()
    assert sut.to_string() == "1.3.0"

    version = Version.from_str("1.3.0", "SNAPSHOT")
    sut = version.create_minor()
    assert sut.to_string() == "1.4.0"


def test_should_create_major():
    version = Version.from_str("1.2.3-SNAPSHOT", "SNAPSHOT")
    sut = version.create_major()
    assert sut.to_string() == "2.0.0"

    version = Version.from_str("1.2.3", "SNAPSHOT")
    sut = version.create_major()
    assert sut.to_string() == "2.0.0"

    version = Version.from_str("1.0.0-SNAPSHOT", "SNAPSHOT")
    sut = version.create_major()
    assert sut.to_string() == "1.0.0"

    version = Version.from_str("1.0.0", "SNAPSHOT")
    sut = version.create_major()
    assert sut.to_string() == "2.0.0"


def test_should_create_bump():
    version = Version.from_str("1.2.3-SNAPSHOT", "SNAPSHOT")
    sut = version.create_bump()
    assert sut.to_string() == "1.2.3-SNAPSHOT"

    version = Version.from_str("1.2.3", "SNAPSHOT")
    sut = version.create_bump()
    assert sut.to_string() == "1.2.4-SNAPSHOT"

    version = Version.from_str("1.0.0", "SNAPSHOT")
    sut = version.create_bump()
    assert sut.to_string() == "1.0.1-SNAPSHOT"

    version = Version.from_str("1.0.0", "dev")
    sut = version.create_bump()
    assert sut.to_string() == "1.0.1-dev"
