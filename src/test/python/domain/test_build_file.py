import pytest
from pathlib import Path
from src.main.python.ddadevops.domain import (
    BuildFileType,
    BuildFile,
    Version,
)


def test_should_validate_build_file():
    sut = BuildFile(Path("./project.clj"), "content")
    assert sut.is_valid()

    sut = BuildFile(None, "")
    assert not sut.is_valid()

    sut = BuildFile(Path("./unknown.extension"), "content")
    assert not sut.is_valid()


def test_should_calculate_build_type():
    sut = BuildFile(Path("./project.clj"), "content")
    assert sut.build_file_type() == BuildFileType.JAVA_CLOJURE

    sut = BuildFile(Path("./build.gradle"), "content")
    assert sut.build_file_type() == BuildFileType.JAVA_GRADLE

    sut = BuildFile(Path("./package.json"), "content")
    assert sut.build_file_type() == BuildFileType.JS


def test_should_parse_and_set_js():
    sut = BuildFile(
        Path("./package.json"),
        """
{
  "name":"c4k-jira",
  "description": "Generate c4k yaml for a jira deployment.",
  "author": "meissa GmbH",
  "version": "1.1.5-SNAPSHOT",
  "homepage": "https://gitlab.com/domaindrivenarchitecture/c4k-jira#readme",
  "bin":{
    "c4k-jira": "./c4k-jira.js"
  }
}
""",
    )
    assert sut.get_version() == Version.from_str("1.1.5-SNAPSHOT", "SNAPSHOT")

    sut = BuildFile(
        Path("./package.json"),
        """
{
  "name":"c4k-jira",
}
""",
    )
    with pytest.raises(Exception):
        sut.get_version()

    sut = BuildFile(
        Path("./package.json"),
        """
{
  "name":"c4k-jira",
  "version": "1.1.5-SNAPSHOT"
}
""",
    )
    sut.set_version(Version.from_str("1.1.5-SNAPSHOT", "SNAPSHOT").create_major())
    assert (
        """{
    "name": "c4k-jira",
    "version": "2.0.0"
}"""
        == sut.content
    )


def test_should_parse_and_set_version_for_gradle():
    sut = BuildFile(
        Path("./build.gradle"),
        """
version = "1.1.5-SNAPSHOT"

""",
    )
    assert sut.get_version() == Version.from_str("1.1.5-SNAPSHOT", "SNAPSHOT")

    sut = BuildFile(
        Path("./build.gradle"),
        """
version = "1.1.5-SNAPSHOT"
""",
    )
    sut.set_version(Version.from_str("1.1.5-SNAPSHOT", "SNAPSHOT").create_major())
    assert '\nversion = "2.0.0"\n' == sut.content


def test_should_parse_and_set_version_for_py():
    sut = BuildFile(
        Path("./build.py"),
        """
from pybuilder.core import init, use_plugin, Author
use_plugin("python.core")

name = "ddadevops"
version = "1.1.5-dev12"
summary = "tools to support builds combining gopass, terraform, dda-pallet, aws & hetzner-cloud"
""",
    )
    assert sut.get_version() == Version.from_str("1.1.5-dev12", "dev")

    sut = BuildFile(
        Path("./build.py"),
        """
version = "1.1.5-dev12"
""",
    )
    sut.set_version(Version.from_str("1.1.5-dev12", "dev").create_major())
    assert '\nversion = "2.0.0"\n' == sut.content

    sut = BuildFile(
        Path("./build.py"),
        """
from pybuilder.core import init, use_plugin, Author
use_plugin("python.core")

name = "ddadevops"
version = "1.1.5-SNAPSHOT"
summary = "tools to support builds combining gopass, terraform, dda-pallet, aws & hetzner-cloud"
""",
    )
    assert sut.get_version() == Version.from_str("1.1.5-SNAPSHOT", "SNAPSHOT")

    sut = BuildFile(
        Path("./build.py"),
        """
version = "1.1.5-SNAPSHOT"
""",
    )
    sut.set_version(Version.from_str("1.1.5-SNAPSHOT", "SNAPSHOT").create_major())
    assert '\nversion = "2.0.0"\n' == sut.content


def test_should_parse_and_set_version_for_clj():
    sut = BuildFile(
        Path("./project.clj"),
        """
(defproject org.domaindrivenarchitecture/c4k-jira "1.1.5-SNAPSHOT"
  :description "jira c4k-installation package"
  :url "https://domaindrivenarchitecture.org"
)
""",
    )
    assert sut.get_version() == Version.from_str("1.1.5-SNAPSHOT", "SNAPSHOT")

    sut = BuildFile(
        Path("./project.clj"),
        """
(defproject org.domaindrivenarchitecture/c4k-jira "1.1.5-SNAPSHOT"
  :description "jira c4k-installation package"
)
""",
    )
    sut.set_version(Version.from_str("1.1.5-SNAPSHOT", "SNAPSHOT").create_major())
    assert (
        '\n(defproject org.domaindrivenarchitecture/c4k-jira "2.0.0"\n  :description "jira c4k-installation package"\n)\n'
        == sut.content
    )

    sut = BuildFile(
        Path("./project.clj"),
        """
(defproject org.domaindrivenarchitecture/c4k-jira "1.1.5-SNAPSHOT"
:dependencies [[org.clojure/clojure "1.11.0"]]
)
    """,
    )
    sut.set_version(Version.from_str("1.1.5-SNAPSHOT", "SNAPSHOT").create_major())
    assert (
        '\n(defproject org.domaindrivenarchitecture/c4k-jira "2.0.0"\n:dependencies [[org.clojure/clojure "1.11.0"]]\n)\n    '
        == sut.content
    )

def test_should_parse_and_set_version_for_clj_edn():
    sut = BuildFile(
        Path("./deps.edn"),
        """
{:project {:name org.domaindrivenarchitecture/dda-backup
           :version "1.1.5-SNAPSHOT"}

}
""",
    )
    assert sut.get_version() == Version.from_str("1.1.5-SNAPSHOT", "SNAPSHOT")

    sut = BuildFile(
        Path("./deps.edn"),
        """
{:project {:name org.domaindrivenarchitecture/dda-backup
           :version "1.1.5-SNAPSHOT"}

}
""",
    )
    sut.set_version(Version.from_str("1.1.5-SNAPSHOT", "SNAPSHOT").create_major())
    assert (
        '\n{:project {:name org.domaindrivenarchitecture/dda-backup\n           :version "2.0.0"}\n\n}\n'
        == sut.content
    )


def test_should_throw_for_clj_wrong_version():
    sut = BuildFile(
        Path("./project.clj"),
        """
(defproject org.domaindrivenarchitecture/c4k-jira "1.1.5-Snapshot"
  :description "jira c4k-installation package"
  :url "https://domaindrivenarchitecture.org"
)
""",
    )

    with pytest.raises(RuntimeError):
        sut.get_version()

def test_should_ignore_first_version_for_py():
    sut = BuildFile(
        Path("./build.py"),
        """
from pybuilder.core import init, use_plugin, Author
use_plugin("python.core")

name = "ddadevops"
project_version = "0.0.2-dev1"
version = "1.1.5-dev12"
summary = "tools to support builds combining gopass, terraform, dda-pallet, aws & hetzner-cloud"
""",
    )
    assert sut.get_version() == Version.from_str("1.1.5-dev12", "dev")

def test_should_ignore_first_version_for_gradle():
    sut = BuildFile(
        Path("./build.gradle"),
        """
kotlin_version = "3.3.3"
version = "1.1.5-SNAPSHOT"

""",
    )
    assert sut.get_version() == Version.from_str("1.1.5-SNAPSHOT", "SNAPSHOT")