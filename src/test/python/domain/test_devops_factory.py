import pytest
from src.main.python.ddadevops.domain import (
    DevopsFactory,
    Version,
    BuildType,
    MixinType,
    Artifact,
)


def test_devops_creation():
    with pytest.raises(Exception):
        DevopsFactory().build_devops({"build_types": ["NOTEXISTING"]})

    with pytest.raises(Exception):
        DevopsFactory().build_devops(
            {
                "build_types": ["IMAGE"],
            }
        )

    sut = DevopsFactory().build_devops(
        {
            "stage": "test",
            "name": "mybuild",
            "module": "test_image",
            "project_root_path": "../../..",
            "build_types": ["IMAGE"],
            "mixin_types": [],
            "image_dockerhub_user": "dockerhub_user",
            "image_dockerhub_password": "dockerhub_password",
            "image_tag": "docker_image_tag",
        }
    )
    assert sut is not None
    assert sut.specialized_builds[BuildType.IMAGE] is not None

    sut = DevopsFactory().build_devops(
        {
            "stage": "test",
            "name": "mybuild",
            "module": "test_image",
            "project_root_path": "../../..",
            "build_types": ["C4K"],
            "mixin_types": [],
            "c4k_grafana_cloud_user": "user",
            "c4k_grafana_cloud_password": "password",
        },
        Version.from_str("1.0.0", "SNAPSHOT"),
    )
    assert sut is not None
    assert sut.specialized_builds[BuildType.C4K] is not None

def test_release_devops_creation():
    sut = DevopsFactory().build_devops(
        {
            "stage": "test",
            "name": "mybuild",
            "module": "test_image",
            "project_root_path": "../../..",
            "build_types": [],
            "mixin_types": ["RELEASE"],
            "release_main_branch": "main",
            "release_current_branch": "my_feature",
            "release_config_file": "project.clj",
        },
        Version.from_str("1.0.0", "SNAPSHOT"),
    )
    assert sut is not None
    assert sut.mixins[MixinType.RELEASE] is not None

    sut = DevopsFactory().build_devops(
        {
            "stage": "test",
            "name": "mybuild",
            "module": "test_image",
            "project_root_path": "../../..",
            "build_types": [],
            "mixin_types": ["RELEASE"],
            "release_main_branch": "main",
            "release_current_branch": "my_feature",
            "release_config_file": "project.clj",
            "release_artifacts": ["x.jar"],
            "release_artifact_token": "y", 
            "release_artifact_server_url": "https://repo.prod.meissa.de",
            "release_organisation": "meissa",
            "release_repository_name": "provs",
        },
        Version.from_str("1.0.0", "SNAPSHOT"),
    )

    release = sut.mixins[MixinType.RELEASE]
    assert release is not None
    assert Artifact("x.jar") == release.release_artifacts[0]


def test_on_merge_input_should_win():
    sut = DevopsFactory()
    assert {'tag': 'inp'} == sut.merge(inp = {'tag': 'inp'}, context = {'tag': 'context'}, authorization={})
