from pybuilder.core import Project
from pathlib import Path
from src.main.python.ddadevops.domain import (
    BuildType,
    Aws,
)
from .helper import devops_config


def test_aws_creation():
    sut = Aws(
        {
            "module": "module",
            "stage": "test",
            "aws_access_key": "aws_access_key",
            "aws_secret_key": "aws_secret_key",
            "aws_account_name": "aws_account_name",
        }
    )
    assert sut is not None
    assert sut.is_valid()

    sut = Aws(
        {
            "module": "module",
            "stage": "test",
            "aws_access_key": "aws_access_key",
            "aws_secret_key": "aws_secret_key",
            "aws_as_backend": True,
            "aws_bucket": "bucket",
            "aws_bucket_kms_key_id": "aws_bucket_kms_key_id",
        }
    )
    assert sut is not None
    assert sut.is_valid()


def test_should_calculate_backend_config():
    sut = Aws(
        devops_config(
            {
                "module": "dns_aws",
                "stage": "prod",
                "aws_access_key": "aws_access_key",
                "aws_secret_key": "aws_secret_key", 
                "aws_bucket": "meissa-configuration",
                "aws_bucket_kms_key_id": "arn:aws:kms:eu-central-1:907507348333:alias/meissa-configuration",
                "aws_region": "eu-central-1",
            }
        )
    )
    assert {
        "access_key": "aws_access_key",
        "secret_key": "aws_secret_key",
        "bucket": "meissa-configuration",
        "key": "prod/dns_aws",
        "kms_key_id": "arn:aws:kms:eu-central-1:907507348333:alias/meissa-configuration",
        "region": "eu-central-1",
    } == sut.backend_config()


def test_should_calculate_project_vars():
    sut = Aws(
        devops_config(
            {
                "aws_as_backend": False,
            }
        )
    )
    assert {
        "aws_access_key": "aws_access_key",
        "aws_secret_key": "aws_secret_key",
        "aws_region": "region",
    } == sut.project_vars()

    sut = Aws(
        devops_config(
            {
                "aws_as_backend": True,
            }
        )
    )
    assert {
        "aws_access_key": "aws_access_key",
        "aws_secret_key": "aws_secret_key",
        "aws_region": "region",
        "account_name": "test",
        "bucket": "bucket",
        "key": "test/module",
        "kms_key_id": "aws_bucket_kms_key_id",
    } == sut.project_vars()
