from pybuilder.core import Project
from pathlib import Path
from src.main.python.ddadevops.domain import (
    Validateable,
    DnsRecord,
    Devops,
    BuildType,
    Version,
    ReleaseType,
    Release,
)
from src.main.python.ddadevops.domain.image import Image
from .helper import build_devops


class MockValidateable(Validateable):
    def __init__(self, value):
        self.field = value

    def validate(self):
        return self.__validate_is_not_empty__("field")


def test_should_validate_non_empty_strings():
    sut = MockValidateable("content")
    assert sut.is_valid()

    sut = MockValidateable(None)
    assert not sut.is_valid()

    sut = MockValidateable("")
    assert not sut.is_valid()


def test_should_validate_non_empty_others():
    sut = MockValidateable(1)
    assert sut.is_valid()

    sut = MockValidateable(1.0)
    assert sut.is_valid()

    sut = MockValidateable(True)
    assert sut.is_valid()

    sut = MockValidateable(None)
    assert not sut.is_valid()


def test_validate_with_reason():
    sut = MockValidateable(None)
    assert sut.validate()[0] == "Field 'field' must not be None."


def test_should_validate_DnsRecord():
    sut = DnsRecord(None)
    assert not sut.is_valid()

    sut = DnsRecord("name")
    assert not sut.is_valid()

    sut = DnsRecord("name", ipv4="1.2.3.4")
    assert sut.is_valid()

    sut = DnsRecord("name", ipv6="1::")
    assert sut.is_valid()
