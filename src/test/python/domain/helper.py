from pathlib import Path
from src.main.python.ddadevops.domain import DevopsFactory, Devops, Version, BuildFile


def devops_config(overrides: dict) -> dict:
    default = {
        "name": "name",
        "module": "module",
        "stage": "test",
        "project_root_path": "root_path",
        "build_dir_name": "target",
        "build_types": ["IMAGE", "C4K", "K3S", "TERRAFORM"],
        "mixin_types": ["RELEASE"],
        "image_dockerhub_user": "dockerhub_user",
        "image_dockerhub_password": "dockerhub_password",
        "image_tag": "image_tag",
        "c4k_config": {},
        "c4k_grafana_cloud_user": "user",
        "c4k_grafana_cloud_password": "password",
        "c4k_grafana_cloud_url": "https://prometheus-prod-01-eu-west-0.grafana.net/api/prom/push",
        "c4k_auth": {},
        "k3s_provision_user": "k3s_provision_user",
        "k3s_letsencrypt_email": "k3s_letsencrypt_email",
        "k3s_letsencrypt_endpoint": "k3s_letsencrypt_endpoint",
        "k3s_enable_echo": "false",
        "k3s_app_filename_to_provision": "k3s_app.yaml",
        "tf_provider_types": ["DIGITALOCEAN", "HETZNER", "AWS"],
        "tf_additional_vars": [],
        "tf_output_json_name": "the_out.json",
        "tf_use_workspace": None,
        "tf_use_package_common_files": None,
        "tf_build_commons_path": "build_commons_path",
        "tf_build_commons_dir_name": "terraform",
        "tf_debug_print_terraform_command": None,
        "tf_additional_tfvar_files": {},
        "tf_terraform_semantic_version": None,
        "do_api_key": "api_key",
        "do_spaces_access_id": "spaces_id",
        "do_spaces_secret_key": "spaces_secret",
        "do_as_backend": True,
        "do_endpoint": "endpoint",
        "do_bucket": "bucket",
        "do_region": "region",
        "hetzner_api_key": "hetzner_api_key",
        "aws_access_key": "aws_access_key",
        "aws_secret_key": "aws_secret_key",
        "aws_as_backend": True,
        "aws_bucket": "bucket",
        "aws_region": "region",
        "aws_bucket_kms_key_id": "aws_bucket_kms_key_id",
        "release_type": "NONE",
        "release_main_branch": "main",
        "release_current_branch": "my_feature",
        "release_primary_build_file": "./package.json",
        "release_secondary_build_file": [],
        "release_artifacts": [],
        "release_artifact_token": "release_artifact_token",
        "release_artifact_server_url": None,
        "release_organisation": None,
        "release_repository_name": None,
        "credentials_mappings": [
            {
                "gopass_path": "a/path",
                "gopass_field": "a-field",
            },
        ],
    }
    input = default.copy()
    input.update(overrides)
    return input


def build_devops(
    overrides: dict, version: Version = Version.from_str("1.0.0-SNAPSHOT", "SNAPSHOT")
) -> Devops:
    return DevopsFactory().build_devops(devops_config(overrides), version=version)


class BuildFileRepositoryMock:
    def __init__(self, file_name=None):
        if file_name:
            self.file_path = Path(f"./src/test/resources/{file_name}")
            with open(self.file_path, "r", encoding="utf-8") as input_file:
                self.content = input_file.read()
        else:
            self.file_path = Path("./package.json")
            self.content = """
{
  "version": "1.1.5-SNAPSHOT"
}
"""

    def get(self, path: Path) -> BuildFile:
        return BuildFile(self.file_path, self.content)

    def write(self, build_file: BuildFile):
        pass


class EnvironmentApiMock:
    def __init__(self, mappings):
        self.mappings = mappings

    def get(self, key):
        return self.mappings.get(key, None)

    def is_defined(self, key):
        return key in self.mappings


class CredentialsApiMock:
    def __init__(self, mappings):
        self.mappings = mappings

    def gopass_field_from_path(self, path, field):
        return self.mappings.get(f"{path}:{field}", None)

    def gopass_password_from_path(self, path):
        return self.mappings.get(path, None)


class GitApiMock:
    def get_latest_n_commits(self, n: int):
        pass

    def get_latest_commit(self):
        pass

    def tag_annotated(self, annotation: str, message: str, count: int):
        pass

    def tag_annotated_second_last(self, annotation: str, message: str):
        pass

    def get_latest_tag(self):
        pass

    def get_current_branch(self):
        pass

    def init(self, default_branch: str = "main"):
        pass

    def set_user_config(self, email: str, name: str):
        pass

    def add_file(self, file_path: Path):
        pass

    def add_remote(self, origin: str, url: str):
        pass

    def commit(self, commit_message: str):
        pass

    def push(self):
        pass

    def push_follow_tags(self):
        pass

    def checkout(self, branch: str):
        pass


class ArtifactDeploymentApiMock:
    def __init__(self, release=""):
        self.release = release
        self.create_forgejo_release_count = 0
        self.add_asset_to_release_count = 0
        self.add_asset_to_release_api_endpoint = ""

    def create_forgejo_release(self, api_endpoint: str, tag: str, token: str):
        self.create_forgejo_release_count += 1
        return self.release

    def add_asset_to_release(
        self, api_endpoint: str, attachment: str, attachment_type: str, token: str
    ):
        self.add_asset_to_release_api_endpoint = api_endpoint
        self.add_asset_to_release_count += 1
        pass

    def calculate_sha256(self, path: Path):
        return f"{path}.sha256"

    def calculate_sha512(self, path: Path):
        return f"{path}.sha512"
