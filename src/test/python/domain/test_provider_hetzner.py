from pybuilder.core import Project
from pathlib import Path
from src.main.python.ddadevops.domain import (
    BuildType,
    Hetzner,
)
from .helper import devops_config


def test_hetzner_creation():
    sut = Hetzner(
        devops_config(
            {
                "hetzner_api_key": "api_key",
            }
        )
    )
    assert sut is not None
    assert sut.is_valid()
