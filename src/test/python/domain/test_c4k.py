import pytest
from pathlib import Path
from src.main.python.ddadevops.domain import (
    DnsRecord,
    BuildType,
    C4k
)
from .helper import build_devops


def test_creation():
    sut = build_devops({})
    assert BuildType.C4K in sut.specialized_builds


def test_c4k_should_calculate_config():
    sut = build_devops({})
    with pytest.raises(Exception):
        sut.specialized_builds[BuildType.C4K].config()

    sut = build_devops({})
    c4k = sut.specialized_builds[BuildType.C4K]
    c4k.update_runtime_config(DnsRecord("fqdn", ipv6="::1"))
    assert {
        "fqdn": "fqdn",
        "mon-cfg": {
            "cluster-name": "module",
            "cluster-stage": "test",
            "grafana-cloud-url": "https://prometheus-prod-01-eu-west-0.grafana.net/api/prom/push",
        },
    } == c4k.config()

    sut = build_devops(
        {
            "c4k_config": {"test": "test"},
        }
    )
    c4k = sut.specialized_builds[BuildType.C4K]
    c4k.update_runtime_config(DnsRecord("fqdn", ipv6="::1"))
    assert {
        "test": "test",
        "fqdn": "fqdn",
        "mon-cfg": {
            "cluster-name": "module",
            "cluster-stage": "test",
            "grafana-cloud-url": "https://prometheus-prod-01-eu-west-0.grafana.net/api/prom/push",
        },
    } == c4k.config()


def test_c4k_should_calculate_auth():
    sut = build_devops({})
    c4k = sut.specialized_builds[BuildType.C4K]
    assert {
        "mon-auth": {"grafana-cloud-password": "password", "grafana-cloud-user": "user"}
    } == c4k.auth()

    sut = build_devops(
        {
            "c4k_auth": {"test": "test"},
        }
    )
    c4k = sut.specialized_builds[BuildType.C4K]
    assert {
        "test": "test",
        "mon-auth": {
            "grafana-cloud-password": "password",
            "grafana-cloud-user": "user",
        },
    } == c4k.auth()


def test_c4k_build_should_calculate_command():
    sut = build_devops(
        {
            "project_root_path": ".",
        }
    )
    assert (
        "c4k-module-standalone.jar "
        + "./target/name/module/out_c4k_config.yaml "
        + "./target/name/module/out_c4k_auth.yaml > "
        + "./target/name/module/out_module.yaml"
        == sut.specialized_builds[BuildType.C4K].command(sut)
    )

    sut = build_devops(
        {
            "project_root_path": ".",
            "c4k_executable_name": "executable_name",
        }
    )
    assert (
        "c4k-executable_name-standalone.jar "
        + "./target/name/module/out_c4k_config.yaml "
        + "./target/name/module/out_c4k_auth.yaml > "
        + "./target/name/module/out_module.yaml"
        == sut.specialized_builds[BuildType.C4K].command(sut)
    )
