import pytest
from pathlib import Path
from src.main.python.ddadevops.domain import DnsRecord, BuildType, K3s
from .helper import build_devops


def test_creation():
    sut = build_devops({})
    assert BuildType.K3S in sut.specialized_builds
    assert sut.specialized_builds[BuildType.K3S]


def test_should_calculate_provs_config():
    sut = build_devops({}).specialized_builds[BuildType.K3S]
    sut.update_runtime_config(DnsRecord("example.org", ipv6="::1"))
    assert "fqdn:" in sut.provs_config()
    assert not "$" in sut.provs_config()


def test_should_calculate_command():
    devops = build_devops({})
    sut = devops.specialized_builds[BuildType.K3S]
    sut.update_runtime_config(DnsRecord("example.org", ipv6="::1"))
    assert (
        "provs-server.jar "
        + "k3s "
        + "k3s_provision_user@::1 "
        + "-c "
        + "root_path/target/name/module/out_k3sServerConfig.yaml "
        + "-a "
        + "root_path/target/name/module/k3s_app.yaml"
        == sut.command(devops)
    )
