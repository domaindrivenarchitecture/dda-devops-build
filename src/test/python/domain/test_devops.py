import pytest
from src.main.python.ddadevops.domain import (
    Devops,
)
from .helper import build_devops

def test_devops_buildpath():
    sut = build_devops({'module': "cloud", 'name': "meissa"})
    assert "root_path/target/meissa/cloud" == sut.build_path()
