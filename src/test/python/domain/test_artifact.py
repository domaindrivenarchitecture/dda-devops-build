import pytest
from pybuilder.core import Project
from pathlib import Path
from src.main.python.ddadevops.domain import (
    Validateable,
    DnsRecord,
    Devops,
    BuildType,
    MixinType,
    Artifact,
    Image,
)
from .helper import build_devops, devops_config


def test_should_validate_release():
    sut = Artifact("x")
    assert sut.is_valid()

    sut = Artifact(None)
    assert not sut.is_valid()

def test_should_calculate_type():
    sut = Artifact("x.jar")
    assert "application/x-java-archive" == sut.type()

    sut = Artifact("x.js")
    assert "application/x-javascript" == sut.type()

    sut = Artifact("x.jar.sha256")
    assert "text/plain" == sut.type()

