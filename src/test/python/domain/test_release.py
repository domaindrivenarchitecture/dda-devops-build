import pytest
from pybuilder.core import Project
from pathlib import Path
from src.main.python.ddadevops.domain import (
    Validateable,
    DnsRecord,
    Devops,
    BuildType,
    MixinType,
    Version,
    ReleaseType,
    Release,
    Image,
)
from .helper import build_devops, devops_config


def test_should_validate_release():
    sut = Release(
        devops_config(
            {
                "release_type": "MINOR",
                "release_current_branch": "main",
            }
        ),
        Version.from_str("1.3.1-SNAPSHOT", "SNAPSHOT"),
    )
    assert sut.is_valid()

    sut = Release(
        devops_config(
            {
                "release_type": "MINOR",
                "release_current_branch": "some-feature-branch",
            }
        ),
        Version.from_str("1.3.1-SNAPSHOT", "SNAPSHOT"),
    )
    assert not sut.is_valid()

    sut = Release(
        devops_config(
            {
                "release_primary_build_file": 1,
            }
        ),
        Version.from_str("1.3.1-SNAPSHOT", "SNAPSHOT"),
    )
    assert not sut.is_valid()


def test_should_calculate_build_files():
    sut = Release(
        devops_config(
            {
                "release_type": "MINOR",
                "release_current_branch": "main",
                "release_primary_build_file": "project.clj",
                "release_secondary_build_files": ["package.json"],
            }
        ),
        Version.from_str("1.3.1-SNAPSHOT", "SNAPSHOT"),
    )
    assert ["project.clj", "package.json"] == sut.build_files()


def test_should_calculate_forgejo_release_api_endpoint():
    sut = Release(
        devops_config(
            {
                "release_artifacts": [],
                "release_artifact_token": "y", 
                "release_artifact_server_url": "https://repo.prod.meissa.de",
                "release_organisation": "meissa",
                "release_repository_name": "provs",
            }
        ),
        Version.from_str("1.3.1-SNAPSHOT", "SNAPSHOT"),
    )
    assert (
        "https://repo.prod.meissa.de/api/v1/repos/meissa/provs/releases"
        == sut.forgejo_release_api_endpoint()
    )

    sut = Release(
        devops_config(
            {
                "release_artifacts": ["x"],
                "release_artifact_token": "y", 
                "release_artifact_server_url": "https://repo.prod.meissa.de/",
                "release_organisation": "/meissa/",
                "release_repository_name": "provs",
            }
        ),
        Version.from_str("1.3.1-SNAPSHOT", "SNAPSHOT"),
    )
    assert (
        "https://repo.prod.meissa.de/api/v1/repos/meissa/provs/releases"
        == sut.forgejo_release_api_endpoint()
    )
    assert(
        "/meissa/"
        == sut.release_organisation 
    )

    with pytest.raises(Exception):
        sut = Release(
            devops_config(
                {
                    "release_artifact_server_url": "https://repo.prod.meissa.de",
                    "release_organisation": None,
                    "release_repository_name": "provs",
                }
            ),
            Version.from_str("1.3.1-SNAPSHOT", "SNAPSHOT"),
        )
        sut.forgejo_release_api_endpoint()

def test_should_calculate_forgejo_release_asset_api_endpoint():
    sut = Release(
        devops_config(
            {
                "release_artifacts": ["x"],
                "release_artifact_token": "y", 
                "release_artifact_server_url": "https://repo.prod.meissa.de",
                "release_organisation": "meissa",
                "release_repository_name": "provs",
            }
        ),
        Version.from_str("1.3.1-SNAPSHOT", "SNAPSHOT"),
    )
    assert (
        "https://repo.prod.meissa.de/api/v1/repos/meissa/provs/releases/123/assets"
        == sut.forgejo_release_asset_api_endpoint(123)
    )
