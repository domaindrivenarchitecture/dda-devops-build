import pytest
from pathlib import Path
from src.main.python.ddadevops.domain import (
    DnsRecord,
    BuildType,
    ProviderType,
    TerraformDomain,
)
from .helper import build_devops, devops_config


def test_creation():
    devops = build_devops({})
    sut = devops.specialized_builds[BuildType.TERRAFORM]
    assert BuildType.TERRAFORM in devops.specialized_builds
    assert sut
    assert sut.providers[ProviderType.DIGITALOCEAN]
    assert sut.providers[ProviderType.HETZNER]
    assert sut.providers[ProviderType.AWS]


def test_should_calculate_output_json_name():
    config = devops_config({})
    sut = TerraformDomain(config)
    assert "the_out.json" == sut.output_json_name()

    config = devops_config({})
    del config["tf_output_json_name"]
    sut = TerraformDomain(config)
    assert "out_module.json" == sut.output_json_name()


def test_should_validate():
    config = devops_config({})
    sut = TerraformDomain(config)
    assert sut.is_valid()

    config = devops_config(
        {
            "do_api_key": "",
        }
    )
    sut = TerraformDomain(config)
    assert not sut.is_valid()

    config = devops_config(
        {
            "aws_account_name": "",
        }
    )
    sut = TerraformDomain(config)
    assert not sut.is_valid()


def test_should_calculate_terraform_build_commons_path():
    config = devops_config({})
    del config["tf_build_commons_path"]
    del config["tf_build_commons_dir_name"]
    sut = TerraformDomain(config)
    assert Path("terraform") == sut.terraform_build_commons_path()

    config = devops_config({})
    sut = TerraformDomain(config)
    assert Path("build_commons_path/terraform") == sut.terraform_build_commons_path()


def test_should_calculate_project_vars():
    config = devops_config(
        {
            "tf_provider_types": [],
        }
    )
    sut = TerraformDomain(config)
    assert {"module": "module", "stage": "test"} == sut.project_vars()

    config = devops_config(
        {
            "do_as_backend": False,
            "aws_as_backend": False,
        }
    )
    sut = TerraformDomain(config)
    assert {
        "module": "module",
        "stage": "test",
        "do_api_key": "api_key",
        "do_spaces_access_id": "spaces_id",
        "do_spaces_secret_key": "spaces_secret",
        "hetzner_api_key": "hetzner_api_key",
        "aws_access_key": "aws_access_key",
        "aws_secret_key": "aws_secret_key",
        "aws_region": "region",
    } == sut.project_vars()


def test_should_calculate_resources_from_package():
    config = devops_config(
        {
            "tf_provider_types": [],
        }
    )
    sut = TerraformDomain(config)
    assert {"versions.tf", "terraform_build_vars.tf"} == sut.resources_from_package()

    config = devops_config(
        {
            "tf_provider_types": ["DIGITALOCEAN"],
            "do_as_backend": False,
        }
    )
    sut = TerraformDomain(config)
    assert {
        "versions.tf",
        "terraform_build_vars.tf",
        "provider_registry.tf",
        "do_provider.tf",
        "do_provider_vars.tf",
    } == sut.resources_from_package()

    sut = TerraformDomain(
        devops_config(
            {
                "tf_provider_types": ["DIGITALOCEAN"],
                "do_as_backend": True,
            }
        )
    )
    assert {
        "versions.tf",
        "terraform_build_vars.tf",
        "provider_registry.tf",
        "do_provider.tf",
        "do_provider_vars.tf",
        "do_backend_vars.tf",
        "do_backend.tf",
    } == sut.resources_from_package()

    config = devops_config({"tf_provider_types": ["HETZNER"]})
    sut = TerraformDomain(config)
    assert {
        "versions.tf",
        "terraform_build_vars.tf",
        "provider_registry.tf",
        "hetzner_provider.tf",
        "hetzner_provider_vars.tf",
    } == sut.resources_from_package()

    config = devops_config(
        {
            "tf_additional_resources_from_package": {"my.file"},
            "do_as_backend": False,
        }
    )
    sut = TerraformDomain(config)
    assert {
        "versions.tf",
        "terraform_build_vars.tf",
        "provider_registry.tf",
        "do_provider.tf",
        "do_provider_vars.tf",
        "provider_registry.tf",
        "hetzner_provider.tf",
        "hetzner_provider_vars.tf",
        "aws_backend.tf",
        "aws_provider.tf",
        "aws_provider_vars.tf",
        "aws_backend_wkms_vars.tf",
        "my.file",
    } == sut.resources_from_package()


def test_should_calculate_local_state_handling():
    sut = TerraformDomain(
        devops_config(
            {
                "tf_provider_types": [],
            }
        )
    )
    assert sut.is_local_state()

    sut = TerraformDomain(
        devops_config(
            {
                "tf_provider_types": ["DIGITALOCEAN"],
                "do_as_backend": True,
            }
        )
    )
    assert not sut.is_local_state()
