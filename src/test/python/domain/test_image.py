from pybuilder.core import Project
from pathlib import Path
from src.main.python.ddadevops.domain import (
    BuildType,
)
from .helper import build_devops


def test_devops_build_commons_path():
    sut = build_devops({})
    image = sut.specialized_builds[BuildType.IMAGE]
    assert image is not None
    assert image.is_valid()
    assert "docker/" == image.build_commons_path()

def test_should_calculate_image_name():
    sut = build_devops({})
    image = sut.specialized_builds[BuildType.IMAGE]
    assert "name" == image.image_name()

    sut = build_devops({'image_naming': "NAME_ONLY"})
    image = sut.specialized_builds[BuildType.IMAGE]
    assert "name" == image.image_name()

    sut = build_devops({'image_naming': "NAME_AND_MODULE"})
    image = sut.specialized_builds[BuildType.IMAGE]
    assert "name-module" == image.image_name()
