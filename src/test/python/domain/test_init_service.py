import pytest
from src.main.python.ddadevops.domain import (
    InitService,
    DevopsFactory,
    Version,
    MixinType,
    BuildType,
)
from .helper import (
    BuildFileRepositoryMock,
    EnvironmentApiMock,
    CredentialsApiMock,
    GitApiMock,
    devops_config,
)


def test_should_load_build_file():
    sut = InitService(
        DevopsFactory(),
        BuildFileRepositoryMock(),
        CredentialsApiMock({
                "server/meissa/grafana-cloud:grafana-cloud-user": "gopass-gfc-user",
                "server/meissa/grafana-cloud": "gopass-gfc-password",
            }),
        EnvironmentApiMock({}),
        GitApiMock(),
    )
    assert (
        Version.from_str("1.1.5-SNAPSHOT", "SNAPSHOT")
        == sut.initialize(devops_config({})).mixins[MixinType.RELEASE].version
    )

    sut = InitService(
        DevopsFactory(),
        BuildFileRepositoryMock("build.py"),
        CredentialsApiMock({
                "server/meissa/grafana-cloud:grafana-cloud-user": "gopass-gfc-user",
                "server/meissa/grafana-cloud": "gopass-gfc-password",
            }),
        EnvironmentApiMock({}),
        GitApiMock(),
    )
    assert (
        Version.from_str("4.0.0-dev73", "dev")
        == sut.initialize(devops_config({})).mixins[MixinType.RELEASE].version
    )


def test_should_resolve_passwords():
    sut = InitService(
        DevopsFactory(),
        BuildFileRepositoryMock(),
        CredentialsApiMock(
            {
                "server/meissa/grafana-cloud:grafana-cloud-user": "gopass-gfc-user",
                "server/meissa/grafana-cloud": "gopass-gfc-password",
            }
        ),
        EnvironmentApiMock({"C4K_GRAFANA_CLOUD_USER": "env-gfc-user"}),
        GitApiMock(),
    )
    config = devops_config({})
    del config["c4k_grafana_cloud_user"]
    del config["c4k_grafana_cloud_password"]
    devops = sut.initialize(config)
    c4k = devops.specialized_builds[BuildType.C4K]
    assert {
        "mon-auth": {
            "grafana-cloud-password": "gopass-gfc-password",
            "grafana-cloud-user": "env-gfc-user",
        }
    } == c4k.auth()
