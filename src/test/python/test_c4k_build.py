import os
from pybuilder.core import Project
from src.main.python.ddadevops.domain import DnsRecord
from src.main.python.ddadevops.c4k_build import C4kBuild
from .domain.helper import (
    CredentialsApiMock,
    devops_config,
)


def test_c4k_build(tmp_path):
    str_tmp_path = str(tmp_path)
    project = Project(str_tmp_path, name="name")

    os.environ["C4K_GRAFANA_CLOUD_USER"] = "user"
    os.environ["C4K_GRAFANA_CLOUD_PASSWORD"] = "password"

    sut = C4kBuild(
        project,
        devops_config(
            {
                "project_root_path": str_tmp_path,
                "mixin_types": [],
                "build_types": ["C4K"],
                "module": "c4k-test",
                "c4k_config": {"a": 1, "b": 2},
                "c4k_auth": {"c": 3, "d": 4},
                "c4k_grafana_cloud_user": "user",
                "c4k_grafana_cloud_password": "password",
            }
        ),
    )

    sut.initialize_build_dir()
    assert sut.build_path() == f"{str_tmp_path}/target/name/c4k-test"

    sut.update_runtime_config(DnsRecord("test.de", ipv6="::1"))
    sut.write_c4k_config()
    assert os.path.exists(f"{sut.build_path()}/out_c4k_config.yaml")

    sut.write_c4k_auth()
    assert os.path.exists(f"{sut.build_path()}/out_c4k_auth.yaml")
