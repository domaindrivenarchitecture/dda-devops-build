import os
from pathlib import Path
from pybuilder.core import Project
from src.main.python.ddadevops import DevopsBuild
from .domain.helper import devops_config
from .resource_helper import copy_resource


def test_devops_build(tmp_path):
    str_tmp_path = str(tmp_path)
    copy_resource(Path("package.json"), tmp_path)
    project = Project(str_tmp_path, name="name")

    devops_build = DevopsBuild(
        project,
        devops_config(
            {
                "project_root_path": str_tmp_path,
                "build_types": [],
                "mixin_types": [],
            }
        ),
    )
    devops_build.initialize_build_dir()
    assert os.path.exists(f"{devops_build.build_path()}")
