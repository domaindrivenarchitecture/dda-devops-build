(defproject org.domaindrivenarchitecture/c4k-website "1.1.3"
  :description "website c4k-installation package"
  :url "https://domaindrivenarchitecture.org"
  :license {:name "Apache License, Version 2.0"
            :url "https://www.apache.org/licenses/LICENSE-2.0.html"})