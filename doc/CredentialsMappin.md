# CredentialsMapping

Credentials used for builds can be received from various places during build object initialization.
Precedence is
1. if there is a ENV, that is used
2. if there is a gopass_path only is given, the gopass password from path is used
3. if there is a gopass_path and gopass_field is given, the field from path is used.



```mermaid
classDiagram
    class Credentials {
        <<AggregateRoot>>
    }
    class CredentialMapping {
        name
        gopass_path
        gopass_field
        gopass_type()
        name_for_input() - name used in context of build (according to python conventions converted to lower case snake case)
        name_for_environment () - name used in context of ENV (according to ENV conventions converted to upper case snake case)
    }

    Credentials *-- "0..n" CredentialMapping: mappings[name]
    Credentials *-- "0..n" CredentialMapping: default_mappings
```

## Input

| name         | description                                              | default |
| ------------ | -------------------------------------------------------- | ------- |
| gopass_path  | path of secret in gopass                                 | -       |
| gopass_field | optional: if given, the field is read, the password else | -       |
| name         | name in context of build & ENV                           | -       |  |

## Example Usage

### Example project

A complete example project you can find on: https://repo.prod.meissa.de/meissa/buildtest

### Example of a build.py

```python
from os import environ
from pybuilder.core import task, init
from ddadevops import *

name = 'my-project'
MODULE = 'my-module'
PROJECT_ROOT_PATH = '..'

class MyBuild(DevopsTerraformBuild, C4kBuild, ProvsK3sBuild):
    pass

@init
def initialize(project):
    project.build_depends_on("ddadevops>=4.0.0")
    stage = environ["STAGE"]
    
    if stage == "test":
        tmp_letsencrypt_endpoint = "staging"
        tmp_jitsi_secrets_path = "server/jitsi-test"
    elif stage == "prod":
        tmp_letsencrypt_endpoint = "prod"
        tmp_jitsi_secrets_path = "server/jitsi-prod"

    c4k_config = {"issuer": tmp_letsencrypt_endpoint}
    c4k_auth = {
        "jvb-auth-password": gopass_field_from_path(
            tmp_jitsi_secrets_path, "jvb-auth-password"
        ),
        "jicofo-auth-password": gopass_field_from_path(
            tmp_jitsi_secrets_path, "jvb-auth-password"
        ),
        "jicofo-component-secret": gopass_field_from_path(
            tmp_jitsi_secrets_path, "jicofo-component-secret"
        ),
    }

    config = {
        "credentials_mapping": [
            {
                "gopass_path": environ.get("DIGITALOCEAN_TOKEN_KEY_PATH", None),
                "name": "do_api_key",
            },
            {
                "gopass_path": environ.get("HETZNER_API_KEY_PATH", None),
                "name": "hetzner_api_key",
            },
        ],
        "name": name,
        "module": MODULE,
        "stage": stage,
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": ["TERRAFORM", "C4K", "K3S"],
        "mixin_types": [],
        "tf_provider_types": ["DIGITALOCEAN", "HETZNER"],
        "tf_use_workspace": False,
        "tf_terraform_semantic_version": "1.4.2",
        "do_as_backend": True,
        "do_bucket": "my-configuration",
        "k3s_app_filename_to_provision": "out_jitsi.yaml",
        "k3s_letsencrypt_endpoint": tmp_letsencrypt_endpoint,
        "k3s_letsencrypt_email": "admin@your.domain",
        "c4k_config": c4k_config,
        "c4k_auth": c4k_auth,
    }

    build = MyBuild(project, config)
    build.initialize_build_dir()



```
