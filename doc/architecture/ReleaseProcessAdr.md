# ADR: Release process - in DevOps

## Status

accepted

## Context

We discussed how we will handle releases in cooperation with gitlab-ci.

## Decision
### Legend
* B. User:
* Z: Time passes / systems do something:
* E: Events:

### Outcome of Eventstroming: Events ordered by time

* B: is the human devops
* S: is the build / ci system

```mermaid
stateDiagram-v2
    state prepare_release {
        state "B: Pulls the latest changes" as pull
        state "B: Possibly merge/rebase with main" as merge
        state "B: starts 'create release-notes'" as rn
        state "B: commits his changes with [skip-ci]." as c1

        [*] --> pull
        pull --> merge
        merge --> rn
        rn --> c1
        c1 --> [*]
    }
    state release {
        state "B: starts the release build and specifies major, minor, patch" as trigger
        state "S: does a git fetch & status and checks if there are no changes at origin" as fetch
        state "S: starts tests" as test
        state "S: runs the linting" as lint
        state "S: possibly does image building and image testing" as image
        state "S: version numbers are adjusted in project.clj/package.json to full version" as vno
        state "S: change commit is tagged with git tag" as c2
        state "S: version numbers are adjusted in project.clj/package.json to next snapshot " as snap
        state "S: makes a bump commit with [skip-ci]." as c3
        state "S: push to gitlab/gitea along with git tags" as push

        [*] --> trigger
        trigger --> fetch
        fetch --> lint
        fetch --> test
        fetch --> image
        test --> vno
        lint --> vno
        image --> vno
        vno --> c2
        c2 --> snap
        snap --> c3
        c3 --> push
        push --> [*]
    }
    state ci_tag {
        state "S: CI starts - for a new tag" as ct1
        state "S: runs the linting" as ct2
        state "S: CI runs tests" as ct3
        state "S: makes artifacts" as ct4
        state "S: possibly performs image building and image testing" as ct5
        state "S: publishes images and artifacts" as ct6
        

        [*] --> ct1
        ct1 --> ct2
        ct2 --> ct3
        ct3 --> ct4
        ct4 --> ct5
        ct5 --> ct6
        ct6 --> [*]
    }
    state ci_version_bump {
        state "S: CI starts - for push with the last commit" as cvb1
        state "S: CI runs tests" as cvb2
        state "S: performs the linting" as cvb3

        [*] --> cvb1
        cvb1 --> cvb2
        cvb2 --> cvb3
        cvb3 --> [*]
    }
    
    [*] --> prepare_release
    prepare_release --> release
    release --> ci_tag
    release --> ci_version_bump
    ci_tag --> [*]
    ci_version_bump --> [*]

```

## Consequences

1. we implement the release process by using pyb
2. we do test & linting local
3. we do all git operations local