# Devops Frontend with application and domain

```mermaid
classDiagram
    class DevopsBuild {
        __init__(project, config)
        do_sth(project)
    }

    class ProjectRepository {
        get_devops(project): Devops
        set_devops(project, build)
    }

    class Devops

    class BuildService {
        do_sth(project, build)
    }

    DevopsBuild *-- BuildService
    BuildService *-- ProjectRepository
    DevopsBuild *-- ProjectRepository
    
```

In case of simple operations we will not need the BuildService in between.


## Init Sequence

```mermaid
sequenceDiagram
    MyBuild ->> DevOpsBuild: create_config
    MyBuild ->> DevOpsBuild: __init__(project, config)
    activate DevOpsBuild
    DevOpsBuild ->> Devops: __init__
    DevOpsBuild ->> ProjectRepository: set_devops(build)
    deactivate DevOpsBuild
```

## do_sth Sequence

```mermaid
sequenceDiagram
    MyBuild ->> DevOpsBuild: do_sth(project)
    activate DevOpsBuild
    DevOpsBuild ->> BuildService: do_sth(project)
    activate BuildService
    BuildService ->> ProjectRepository: get_devops
    BuildService ->> BuildService: do_some_complicated_stuff(build)
    deactivate BuildService
    deactivate DevOpsBuild
```