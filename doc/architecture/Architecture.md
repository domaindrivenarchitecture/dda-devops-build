# Architecture


```mermaid
    C4Context
      title Architectrue od dda-devops-build
      
      Component(buildAndMixin, "Build and Mixin", "")
      Component(app, "Application", "")
      Component(dom, "Domain", "")
      Component(infra, "Infrastructure", "")

      Rel(buildAndMixin,app, "use")
      Rel(buildAndMixin,dom, "use")
      Rel(app, dom, "use")
      Rel(app, infra, "use")
      Rel(infra, dom, "use")

      UpdateLayoutConfig($c4ShapeInRow="2", $c4BoundaryInRow="1")
      
```