# Domain

```mermaid
classDiagram
    class Devops {
        <<AggregateRoot>>
        name
        module
        stage
        build_dir_name
        project_root_path
    }

    class Image {
        image_naming
        image_dockerhub_user
        image_dockerhub_password
        image_publish_tag
        image_build_dir_name
        image_use_package_common_files
        image_build_commons_path
        image_build_commons_dir_name
    }

    class C4k {
        c4k_executabel_name
        c4k_mixin_config
        c4k_mixin_auth
        c4k_grafana_cloud_user
        c4k_grafana_cloud_password
    }

    class ProvsK3s {
        k3s_provision_user
        k3s_letsencrypt_email
        k3s_letsencrypt_endpoint
        k3s_enable_echo
        k3s_app_filename_to_provision
    }

    class TerraformDomain {
        tf_additional_vars
        tf_additional_resources_from_package
        tf_output_json_name
        tf_use_workspace
        tf_use_package_common_files
        tf_build_commons_path
        tf_commons_dir_name
        tf_debug_print_terraform_command
        tf_additional_tfvar_files
        tf_terraform_semantic_version
    }

    class ProviderDigitalocean {
        do_api_key
        do_spaces_access_key
        do_spaces_secret_key
        do_as_backend
        do_account_name
        do_endpoint
        do_bucket
        do_bucket_key
        do_region
    }

    class ProviderHetzner {
        hetzner_api_key
    }

    class ProviderAws {
        aws_access_key
        aws_secret_key
        aws_bucket
        aws_bucket_kms_key_id
        aws_account_name
        aws_bucket_key
        aws_as_backend
        aws_region
    }
    
    class DnsRecord {
        fqdn
        ipv4
        ipv6
    }

    class Release {
        release_type
        release_main_branch
        release_current_branch
        release_artifact_server_url
        release_organisation
        release_repository_name
        release_artifact_token
    }
    class Artifact {
        path_str
        path()
        type()
    }
    class Credentials {
        <<AggregateRoot>>
    }
    class CredentialMapping {
        name
        gopass_path
        gopass_field
        gopass_type()
        name_for_input()
        name_for_environment ()
    }


    class BuildFile {
        <<AggregateRoot>>
        file_path [id]
        content
        build_file_type()
        get_version()
        set_version(version)
    }
    
    class Version {
        to_string()
        create_major()
        create_minor()
        create_patch()
        create_bump(snapshot_suffix)
    }


    Devops *-- "0..1" Image: specialized_builds
    Devops *-- "0..1" C4k: specialized_builds
    Devops *-- "0..1" ProvsK3s: specialized_builds
    Devops *-- "0..1" TerraformDomain: specialized_builds
    Devops *-- "0..1" Release: mixins
    TerraformDomain *-- "0..1" ProviderDigitalocean: providers
    TerraformDomain *-- "0..1" ProviderHetzner: providers
    TerraformDomain *-- "0..1" ProviderAws: providers
    Release o-- "0..1" BuildFile: primary_build_file
    Release o-- "0..n" BuildFile: secondary_build_files
    Release "1" *-- "0..n" Artifact: release_artifacts
    Release "1" *-- "1" Version: version
    BuildFile *-- "1" Version: version
    C4k *-- DnsRecord: dns_record
    ProvsK3s *-- DnsRecord: provision_dns
    Credentials *-- "0..n" CredentialMapping: mappings[name]
    Credentials *-- "0..n" CredentialMapping: default_mappings
    
```

# Infrastructure

```mermaid
classDiagram
    class ProjectRepository {
        get_devops(project): Devops
        set_devops(project, build)
    }   
    
```