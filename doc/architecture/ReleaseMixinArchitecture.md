# Architecture of ReleaseMixin

[Link to live editor](https://mermaid.live/edit#pako:eNrtV99vmzAQ_lcsPzUSrUjIj8JDpUqb9jSpaqs9TJGQg6_UGxhmTNes6v8-E0MCGIem6-MiBYnz3Xd3vu8ulxccZRRwgAv4VQKP4BMjsSApUp81r54CIolEvDmbup6D6sdEn21KllB0fnWFbiEBUsBX9sx4gMKQcSbD8CwX2Q9l76Ao4w8sDh9YArUtiSR7IhI6pge3HWnl4QuT1zlrYU8sirXgfpvDLeRZwWQmti07DVRb50RIFrGccNk2tEB_A1GwjA_Cmhm2sWvL4yEP4ho-neEMHZQSxsONIDx6tGenD4BTo7oO8tV3NVLaXIBChVBoaVOFPc7MrfixcNDCtRXoRkPU8jsQTyyCVsbGbfQZMwigdQaPbHccg-zn0WflQb2TzEF8jHIt_FCqM5uTrl3HUfeo0wgVeqJQChlGWZoyacBrTS2kMCi2u2mdBOjQly2c8eh7kAPtUyXxpMVG-Ia6PjfENuwkI3TXjw3ymy0VhVTJ3mza4m5l46A6ozBhhZwY92bJ6yi1zPaort1psNSALYUALrv9v29zs2p972Pn4wgfMAJ-CyYhJJzWjO5352h3B6jpNxupOmOwfunWMhKgFEMD6FgPjIVnHXlGxo27OpzJO8baiS2oQ9iRveFtIQXj8ajvZhIRSs_erNydVeYP0QeyZ1Om-QnUqdSHyn06d2xI_4nzYcRpXeWRdWBPr4GFpyLaym3xzLbySBLvLjovi8fDRDqyqt6T-JrTG6Vu3XE6S-g-E5vhu3wNh61hrI7GIU9BakqnQ-GZVEnSf4zh5HSaICrDAfbYbG0du7v6HqmqJ3ZwCkLt4FT9m3qpJGssHyGFNVadhSkRP9d4zV-VHilldrflEQ4eSFKAg8ucKg_1X6-e9DOt2m0vVLv89yxTSlKU-hUHL_gZB-fT6cWlt_Td5dzz1ACdL30Hb5Xcny4uZjN_7k2ni5k39y5fHfxnBzG7cD135fnzxdJfrObu6vUveFPSvA)

```mermaid
sequenceDiagram    
    rect rgb(103, 103, 10)
    build ->> ReleaseMixin: __init__(project, config_file)
    activate ReleaseMixin    
    ReleaseMixin ->> GitApi: __init__()
    ReleaseMixin ->> ReleaseTypeRepository: __init__(GitApi)
    participant ReleaseType
    ReleaseMixin ->> VersionRepository: __init__(config_file)
    participant Version
    ReleaseMixin ->> ReleaseRepository: __init__(VersionRepository, ReleaseTypeRepository, main_branch)
    participant Release
    end
    rect rgb(10, 90, 7)
    build ->> ReleaseMixin: prepare_release()
    rect rgb(20, 105, 50)
    ReleaseMixin ->> PrepareReleaseService: __init__(ReleaseRepository)
    activate PrepareReleaseService
    PrepareReleaseService ->> ReleaseRepository: get_release()
    activate ReleaseRepository
    ReleaseRepository ->> ReleaseTypeRepository: get_release_type()
    activate ReleaseTypeRepository
    ReleaseTypeRepository ->> GitApi: get_latest_commit()
    activate GitApi
    deactivate GitApi
    ReleaseTypeRepository ->> ReleaseType:     
    deactivate ReleaseTypeRepository
    ReleaseRepository ->> VersionRepository: get_version()
    activate VersionRepository
    VersionRepository ->> VersionRepository: load_file()
    VersionRepository ->> VersionRepository: parse_file()
    VersionRepository ->> Version: __init__(file, version_list)
    deactivate VersionRepository
    ReleaseRepository ->> Release: __init__(ReleaseType, Version, current_branch)
    end
    deactivate ReleaseRepository
    activate ReleaseRepository
    deactivate ReleaseRepository
    rect rgb(20, 105, 50)
    ReleaseMixin ->> PrepareReleaseService: write_and_commit_release()
    PrepareReleaseService ->> Release: release_version()
    activate Release
    Release ->> Version: create_release_version()    
    deactivate Release
    PrepareReleaseService ->> PrepareReleaseService: __write_and_commit_version(Version)
    PrepareReleaseService ->> ReleaseRepository: 
    ReleaseRepository ->> VersionRepository: write_file(version_string)
    PrepareReleaseService ->> GitApi: add()
    PrepareReleaseService ->> GitApi: commit()
    end
    rect rgb(20, 105, 50)
    ReleaseMixin ->> PrepareReleaseService: write_and_commit_bump()
    PrepareReleaseService ->> Release: bump_version()    
    activate Release
    Release ->> Version: create_bump_version()    
    deactivate Release
    PrepareReleaseService ->> PrepareReleaseService: __write_and_commit_version(Version)
    PrepareReleaseService ->> ReleaseRepository: 
    ReleaseRepository ->> VersionRepository: write_file(version_string)
    PrepareReleaseService ->> GitApi: add()
    PrepareReleaseService ->> GitApi: commit()
    deactivate PrepareReleaseService
    end
    end
    rect rgb(120, 70, 50)
    build ->> ReleaseMixin: tag_and_push_release()    
    ReleaseMixin ->> TagAndPushReleaseService: __init__(GitApi)
    activate TagAndPushReleaseService    
    ReleaseMixin ->> TagAndPushReleaseService: tag_and_push_release()
    TagAndPushReleaseService ->> TagAndPushReleaseService: tag_release()
    TagAndPushReleaseService ->> GitApi: tag_annotated()
    TagAndPushReleaseService ->> TagAndPushReleaseService: push_release()
    TagAndPushReleaseService ->> GitApi: push()
    deactivate TagAndPushReleaseService
    deactivate ReleaseMixin
    end
```