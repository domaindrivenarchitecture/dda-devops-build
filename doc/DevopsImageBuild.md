# DevopsImageBuild

DevopsImageBuild provides functionality for building container images.

```mermaid
classDiagram
    class DevopsImageBuild {
        drun() - start your localy build image
        dockerhub_login() - login to dockerhub
        dockerhub_publish() - publish your image
        image() - build image localy. Sources for image build are expected at ./image
        initialize_build_dir() - copy current directory & additional files to target. Additional files can be found [here](../src/main/resources/docker)
        test() - builds a test image. Sources for image test are expected at ./test
    }

```

## Input

| name                     | description                                                                                        | default                                                      |
| ------------------------ | -------------------------------------------------------------------------------------------------- | ------------------------------------------------------------ |
| name                     | dedicated name of the build - also used as image name                                              | module                                                       |
| module                   | module name - may result in a hierarchy like name/module                                           |                                                              |
| stage                    | not used in this build                                                                             |                                                              |
| project_root_path        | relative path to projects root. Is used to locate the target dir                                   |                                                              |
| build_dir_name           | name of dir, build is executed in                                                                  | target                                                       |
| build_types              | list of special builds used. Value has to be contained["IMAGE"]                                    | []                                                           |
| mixin_types              | mixins are orthoganl to builds and represent additional capabilities. Valid Values are ["RELEASE"] | []                                                           |
| credentials_mappings     | values for image_dockerhub_user & image_dockerhub_password can be read from gopass                 | [defaults](#credentials-mapping-defaults)                    |
| image_dockerhub_user     | user to access docker-hub                                                                          | IMAGE_DOCKERHUB_USER from env or credentials from gopass     |
| image_dockerhub_password | password to access docker-hub                                                                      | IMAGE_DOCKERHUB_PASSWORD from env or credentials from gopass |
| image_tag                | tag for publishing the image                                                                       | IMAGE_TAG from env                                           |
| image_naming                | Strategy for calculate the image name. Posible values are [NAME_ONLY,NAME_AND_MODULE] |NAME_ONLY                                          |

### Credentials Mapping defaults

```python
[
    {
        "gopass_path": "meissa/web/docker.com",
        "gopass_field": "login",
        "name": "image_dockerhub_user",
    },
    {
        "gopass_path": "meissa/web/docker.com",
        "name": "image_dockerhub_password",
    },
]
```

## Example Usage
### build.py

```python
from os import environ
from pybuilder.core import task, init
from ddadevops import *

name = 'my-project'
MODULE = 'my-module'
PROJECT_ROOT_PATH = '..'

@init
def initialize(project):
    project.build_depends_on("ddadevops>=4.0.0")

    input = {
        "name": name,
        "module": MODULE,
        "stage": "notused",
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": ["IMAGE"],
        "mixin_types": [],
    }

    build = DevopsImageBuild(project, config)
    build.initialize_build_dir()


@task
def image(project):
    build = get_devops_build(project)
    build.image()


@task
def drun(project):
    build = get_devops_build(project)
    build.drun()


@task
def test(project):
    build = get_devops_build(project)
    build.test()


@task
def publish(project):
    build = get_devops_build(project)
    build.dockerhub_login()
    build.dockerhub_publish()
```

### call the build

```bash
pyb image test publish
```

### full example

... can be found [here](../infrastructure/devops-build)
