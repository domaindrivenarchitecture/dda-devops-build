# Digitalocean Provider

## Input

| name                 | description                                                                                                                 | default                       |
| -------------------- | --------------------------------------------------------------------------------------------------------------------------- | ----------------------------- |
| do_api_key           | the do api key                                                                                                              |                               |
| do_as_backend        | you can use do s3 as backend state storage                                                                                  | False                         |
| do_spaces_access_id  | in case of backend usage                                                                                                    |                               |
| do_spaces_secret_key | in case of backend usage                                                                                                    |                               |
| do_endpoint          | in case of backend usage                                                                                                    | "fra1.digitaloceanspaces.com" |
| do_region            | in case of backend usage                                                                                                    | "eu-central-1"                |
| do_bucket            | in case of backend usage, the bucket your state is stored in. the url is S3://{do_bucket}/{do_bucket_key}/{do_account_name} |                               |
| do_bucket_key        |                                                                                                                             | {module}                      |
| do_account_name      |                                                                                                                             | {stage}                       |

### Credentials Mapping defaults

```python
[
    {
        "gopass_path": "server/devops/digitalocean/s3",
        "gopass_field": "id",
        "name": "do_spaces_access_id",
    },
    {
        "gopass_path": "server/devops/digitalocean/s3",
        "gopass_field": "secret",
        "name": "do_spaces_secret_key",
    },
]
```

## Example Usage
### build.py

```python
from os import environ
from pybuilder.core import task, init
from ddadevops import *

name = 'my-project'
MODULE = 'my-module'
PROJECT_ROOT_PATH = '..'

@init
def initialize(project):
    project.build_depends_on("ddadevops>=4.0.0")

    input = {
        "credentials_mapping": [
            {
                "gopass_path": environ.get("DIGITALOCEAN_TOKEN_KEY_PATH", None),
                "name": "do_api_key",
            },
        ],
        "name": name,
        "module": MODULE,
        "name": name,
        "module": MODULE,
        "stage": environ["STAGE"],
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": ["TERRAFORM"],
        "mixin_types": [],
        "tf_provider_types": ["DIGITALOCEAN"],
        "tf_use_workspace": False,
        "tf_terraform_semantic_version": "1.4.2",
        "do_as_backend": True,
        "do_bucket": "your-configuration",
    }

    build = DevopsTerraformBuild(project, config)
    build.initialize_build_dir()


@task
def plan(project):
    build = get_devops_build(project)
    build.plan()


@task
def apply(project):
    build = get_devops_build(project)
    build.apply(True)


@task
def destroy(project):
    build = get_devops_build(project)
    build.destroy()


@task
def tf_import(project):
    build = get_devops_build(project)
    build.tf_import(
        "terraform.resource", "providers id"
    )
```

### call the build

```bash
pyb image plan apply destroy
```
