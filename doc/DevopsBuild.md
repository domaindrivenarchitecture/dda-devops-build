# DevopsBuild

DevopsBuild provides the build foundations.

```mermaid
classDiagram
    class DevopsBuild {
        build_path() - the build dir in target
        initialize_build_dir() - copy current directory & additional files to target
        name() - the name of build
    }

```

## Input

| name              | description                                                                                        | default |
| ----------------- | -------------------------------------------------------------------------------------------------- | ------- |
| name              | dedicated name of the build                                                                        | module  |
| module            | module name - may result in a hierarchy like name/module                                           |         |
| stage             | sth. like test, int, acc or prod                                                                   |         |
| project_root_path | relative path to projects root. Is used to locate the target dir                                   |         |
| build_dir_name    | name of dir, build is executed in                                                                  | target  |
| build_types       | list of special builds used. Valid values are ["IMAGE", "C4K", "K3S", "TERRAFORM"]                 | []      |
| mixin_types       | mixins are orthoganl to builds and represent additional capabilities. Valid Values are ["RELEASE"] | []      |

## Example Usage

### build.py

```python
from subprocess import run
from pybuilder.core import task, init
from ddadevops import *

name = 'my-project'
MODULE = 'my-module'
PROJECT_ROOT_PATH = '..'

@init
def initialize(project):
    project.build_depends_on("ddadevops>=4.0.0")

    config = {
        "name": name,
        "module": MODULE,
        "stage": environ["STAGE"],
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": [],
        "mixin_types": [],
    }

    build = DevopsBuild(project, config)
    build.initialize_build_dir()


@task
def list_build_dir(project):
    build = get_devops_build(project)
    run(f"ls -la {build.build_path()}")
```

### call the build

```bash
pyb list_build_dir
```
