# DevopsTerraformBuild

DevopsTerraformBuild provides capabilities to use terraform in your build.

```mermaid
classDiagram
    class DevopsTerraformBuild {
        apply(auto_approve=False) - execute terraform apply
        destroy(auto_approve=False) - execute terraform destroy
        initialize_build_dir() - copy current directory & additional files to target. Additional files can be found [here](../src/main/resources/terraform)
        post_build() - a post build hook
        plan() - execute terraform plan
        plan_fail_on_diff() - execute terraform plan and throw an exception if there are diffs
        read_output_json() - read your json encoded output and parse into a nested map
        refresh() - execute terraform refresh
        tf_import(tf_import_name,tf_import_resource) - execute terraform import
    }
```

## Input

| name                             | description                                                                                                                  | default   |
| -------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- | --------- |
| name                             | dedicated name of the build - also used as image name                                                                        | module    |
| module                           | module name - may result in a hierarchy like name/module                                                                     |           |
| stage                            | not used in this build                                                                                                       |           |
| project_root_path                | relative path to projects root. Is used to locate the target dir                                                             |           |
| build_dir_name                   | name of dir, build is executed in                                                                                            | target    |
| build_types                      | list of special builds used. Value has to be contained["IMAGE"]                                                              | []        |
| mixin_types                      | mixins are orthoganl to builds and represent additional capabilities. Valid Values are ["RELEASE"]                           | []        |
| tf_provider_types                | Build in terraform providers. Possible values are ["DIGITALOCEAN", "HETZNER", "AWS"]                                         | []        |
| tf_additional_vars               | hand over vars to your terraform build                                                                                       | {}        |
| tf_additional_tfvar_files        | use additional var files                                                                                                     | []        |
| tf_output_json_name              | name of your output as json                                                                                                  | NONE      |
| tf_use_workspace                 | use terraform workspaces                                                                                                     | True      |
| tf_use_package_common_files      | use some boilerplate files from dda-devops-build located [here](../src/main/resources/terraform)                             | True      |
| tf_build_commons_path            | copy boilerplate files from a {tf_build_commons_path}/{tf_build_commons_dir_name} dir instead of using package-commons files |           |
| tf_build_commons_dir_name        |                                                                                                                              | terraform |
| tf_debug_print_terraform_command | Print out all terraform commands for debuging issues                                                                         | False     |
| tf_terraform_semantic_version    | specify terraform version                                                                                                    | "1.0.8"   |
|                                  |

### Credentials Mapping defaults
#### without a defined provider

```python
[
]
```

## Example Usage
### build.py

```python
from os import environ
from pybuilder.core import task, init
from ddadevops import *

name = 'my-project'
MODULE = 'my-module'
PROJECT_ROOT_PATH = '..'

@init
def initialize(project):
    project.build_depends_on("ddadevops>=4.0.0")

    input = {
        "name": name,
        "module": MODULE,
        "name": name,
        "module": MODULE,
        "stage": environ["STAGE"],
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": ["TERRAFORM"],
        "mixin_types": [],
        "tf_provider_types": [],
        "tf_use_workspace": False,
        "tf_terraform_semantic_version": "1.4.2",
    }

    build = DevopsTerraformBuild(project, config)
    build.initialize_build_dir()


@task
def plan(project):
    build = get_devops_build(project)
    build.plan()


@task
def apply(project):
    build = get_devops_build(project)
    build.apply(True)


@task
def destroy(project):
    build = get_devops_build(project)
    build.destroy()


@task
def tf_import(project):
    build = get_devops_build(project)
    build.tf_import(
        "terraform.resource", "providers id"
    )
```

### call the build

```bash
pyb image plan apply destroy
```
