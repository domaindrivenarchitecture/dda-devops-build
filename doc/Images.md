# ddadevops Images
## ddadevops-clojure

Contains 
* clojure
* shadowcljs
* lein
* java
* graalvm
* pybuilder, ddadevops

## ddadevops

Contains:
* pybuilder, ddadevops

## devops-build

Image is deprecated.

## ddadevops-dind

Contains:
* docker in docker
* pybuilder, ddadevops

## ddadevops-python

Contains:
* python 3.10
* python linting
* python setup-tools
* pybuilder, ddadevops