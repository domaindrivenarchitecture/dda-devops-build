# ReleaseMixin

- [ReleaseMixin](#releasemixin)
  - [Input](#input)
  - [Example Usage just for creating releases](#example-usage-just-for-creating-releases)
    - [build.py](#buildpy)
    - [call the build for creating a major release](#call-the-build-for-creating-a-major-release)
  - [Example Usage for creating a release on forgejo / gitea \& upload the generated artifacts](#example-usage-for-creating-a-release-on-forgejo--gitea--upload-the-generated-artifacts)
    - [build.py](#buildpy-1)
    - [call the build](#call-the-build)


Support for releases following the trunk-based-release flow (see https://trunkbaseddevelopment.com/)

```mermaid
classDiagram
    class ReleaseMixin {
        prepare_release() - adjust all build files to carry the correct version & commit locally
        tag_and_push_release() - tag the git repo and push changes to origin
        update_release_type (release_type) - change the release type during run time
        publish_artifacts() - publish release & artifacts to forgejo/gitea
    }

```

## Input

| name                          | description                                                                                                           | default         |
| ----------------------------- |-----------------------------------------------------------------------------------------------------------------------| --------------- |
| release_type                  | one of MAJOR, MINOR, PATCH, NONE                                                                                      | "NONE"          |
| release_main_branch           | the name of your trunk                                                                                                | "main"          |
| release_primary_build_file    | path to the build file having the leading version info (read & write). Valid extensions are .clj, .json, .gradle, .py | "./project.clj" |
| release_secondary_build_files | list of secondary build files, version is written in.                                                                 | []              |
| release_artifact_server_url   | Optional: The base url of your forgejo/gitea instance to publish a release tode                                       |                 |
| release_organisation          | Optional: The repository organisation name                                                                            |                 |
| release_repository_name       | Optional: The repository name name                                                                                    |                 |
| release_artifacts             | Optional: The list of artifacts to publish to the release generated name                                              | []              |
| release_tag_prefix            | Optional: Prefix of tag                                                                                               | ""              |

## Example Usage just for creating releases

### build.py

```python
from os import environ
from pybuilder.core import task, init
from ddadevops import *

name = 'my-project'
MODULE = 'my-module'
PROJECT_ROOT_PATH = '..'

@init
def initialize(project):
    project.build_depends_on("ddadevops>=4.7.0")

    input = {
        "name": name,
        "module": MODULE,
        "stage": "notused",
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": [],
        "mixin_types": ["RELEASE"],
        "release_type": "MINOR",
        "release_primary_build_file": "project.clj",
        "release_secondary_build_files": ["package.json"],
    }    
    build = ReleaseMixin(project, input)
    build.initialize_build_dir()


@task
def patch(project):
    linttest(project, "PATCH")
    release(project)


@task
def minor(project):
    linttest(project, "MINOR")
    release(project)


@task
def major(project):
    linttest(project, "MAJOR")
    release(project)


@task
def dev(project):
    linttest(project, "NONE")


@task
def prepare(project):
    build = get_devops_build(project)
    build.prepare_release()


@task
def tag(project):
    build = get_devops_build(project)
    build.tag_bump_and_push_release()


def release(project):
    prepare(project)
    tag(project)


def linttest(project, release_type):
    build = get_devops_build(project)
    build.update_release_type(release_type)
    #test(project)
    #lint(project)
```

### call the build for creating a major release

```bash
pyb major
```

## Example Usage for creating a release on forgejo / gitea & upload the generated artifacts

### build.py

```python
rom os import environ
from pybuilder.core import task, init
from ddadevops import *

name = 'my-project'
MODULE = 'my-module'
PROJECT_ROOT_PATH = '..'

@init
def initialize(project):
    project.build_depends_on("ddadevops>=4.7.0")

    input = {
        "name": name,
        "module": MODULE,
        "stage": "notused",
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": [],
        "mixin_types": ["RELEASE"],
        "release_type": "MINOR",
        "release_primary_build_file": "project.clj",
        "release_secondary_build_files": ["package.json"],
        "release_artifact_server_url": "https://repo.prod.meissa.de",
        "release_organisation": "meissa",
        "release_repository_name": "dda-devops-build",
        "release_artifacts": ["target/doc.zip"],
    }
    build = ReleaseMixin(project, input)
    build.initialize_build_dir()

@task
def publish_artifacts(project):
    build = get_devops_build(project)
    build.publish_artifacts()
```

### call the build

```bash
git checkout "4.7.0"
pyb publish_artifacts
```
