# Hetzner Provider



## Input

| name            | description         | default |
| --------------- | ------------------- | ------- |
| hetzner_api_key | the hetzner api key |         |
|                 |

### Credentials Mapping defaults

```python
[
]
```

## Example Usage
### build.py

```python
from os import environ
from pybuilder.core import task, init
from ddadevops import *

name = 'my-project'
MODULE = 'my-module'
PROJECT_ROOT_PATH = '..'

@init
def initialize(project):
    project.build_depends_on("ddadevops>=4.0.0")

    input = {
        "credentials_mapping": [
            {
                "gopass_path": environ.get("HETZNER_API_KEY_PATH", None),
                "name": "hetzner_api_key",
            },
        ],
        "name": name,
        "module": MODULE,
        "name": name,
        "module": MODULE,
        "stage": environ["STAGE"],
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": ["TERRAFORM"],
        "mixin_types": [],
        "tf_provider_types": ["HETZNER"],
        "tf_use_workspace": False,
        "tf_terraform_semantic_version": "1.4.2",
    }

    build = DevopsTerraformBuild(project, config)
    build.initialize_build_dir()


@task
def plan(project):
    build = get_devops_build(project)
    build.plan()


@task
def apply(project):
    build = get_devops_build(project)
    build.apply(True)


@task
def destroy(project):
    build = get_devops_build(project)
    build.destroy()


@task
def tf_import(project):
    build = get_devops_build(project)
    build.tf_import(
        "terraform.resource", "providers id"
    )
```

### call the build

```bash
pyb image plan apply destroy
```
