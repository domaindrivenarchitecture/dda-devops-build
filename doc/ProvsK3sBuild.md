# ProvsK3sBuild

Sets up a single-node k3s cluster using provs.

```mermaid
classDiagram
    class ProvsK3sBuild {
        update_runtime_config(dns_record) - after applying terraform update the dns records
        provs_apply(dry_run=False) - run provs
        write_provs_config() - generate the provs config
    }
```

## Input

| name                              | description                                                       | default   |
| --------------------------------- | ----------------------------------------------------------------- | --------- |
| k3s_provision_user                | the user used to provision k3s                                    | "root"    |
| k3s_letsencrypt_email             | email address used for letsencrypt                                |           |
| k3s_letsencrypt_endpoint          | letsencrypt endpoint. Valid values are staging, prod              | "staging" |
| k3s_app_filename_to_provision     | an k8s manifest to apply imediately after k3s setup was sucessful |           |
| k3s_enable_echo                   | provision the echo app on k3s. Valid values are true, false       | "false"   |
| k3s_provs_template                | use a individual template for provs config                        | None      |
| k3s_enable_hetzner_csi            | enable hetzner csi                                                | False     |
| k3s_hetzner_api_token             | hetzner_api_token                                                 | None      |
| k3s_hetzner_encryption_passphrase | encryption passphrase for volumes                                 | None      |

### Credentials Mapping defaults

```python
[]
```

## Example Usage
### build.py

```python
from os import environ
from pybuilder.core import task, init
from ddadevops import *

name = 'my-project'
MODULE = 'my-module'
PROJECT_ROOT_PATH = '..'

class MyBuild(DevopsTerraformBuild, ProvsK3sBuild):
    pass

@init
def initialize(project):
    project.build_depends_on("ddadevops>=4.0.0")

    stage = environ["STAGE"]
    
    if stage == "test":
        tmp_letsencrypt_endpoint = "staging"
    elif stage == "prod":
        tmp_letsencrypt_endpoint = "prod"

    config = {
        "credentials_mapping": [
            {
                "gopass_path": environ.get("DIGITALOCEAN_TOKEN_KEY_PATH", None),
                "name": "do_api_key",
            },
            {
                "gopass_path": environ.get("HETZNER_API_KEY_PATH", None),
                "name": "hetzner_api_key",
            },
        ],
        "name": name,
        "module": MODULE,
        "stage": stage,
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": ["TERRAFORM", "C4K", "K3S"],
        "mixin_types": [],
        "tf_provider_types": ["DIGITALOCEAN", "HETZNER"],
        "tf_use_workspace": False,
        "tf_terraform_semantic_version": "1.4.2",
        "do_as_backend": True,
        "do_bucket": "my-configuration",
        "k3s_app_filename_to_provision": "my_k8s_application_manifest.yaml",
        "k3s_letsencrypt_endpoint": tmp_letsencrypt_endpoint,
        "k3s_letsencrypt_email": "admin@my.doamin",
    }

    build = MyBuild(project, config)
    build.initialize_build_dir()


@task
def plan(project):
    build = get_devops_build(project)
    build.plan()


@task
def tf_apply(project):
    build = get_devops_build(project)
    build.apply(True)


@task
def show_config(project):
    build = get_devops_build(project)
    build.apply(True)
    out_json = build.read_output_json()
    build.update_runtime_config(
        DnsRecord(
            fqdn=out_json["fqdn"]["value"],
            ipv4=out_json["ipv4"]["value"],
            ipv6=out_json["ipv6"]["value"],
        )
    )
    build.write_provs_config()
    build.provs_apply(dry_run=True)


@task
def apply(project):
    build = get_devops_build(project)
    build.apply(True)
    out_json = build.read_output_json()
    build.update_runtime_config(
        DnsRecord(
            fqdn=out_json["fqdn"]["value"],
            ipv4=out_json["ipv4"]["value"],
            ipv6=out_json["ipv6"]["value"],
        )
    )
    build.write_provs_config()
    time.sleep(5)
    build.provs_apply()


@task
def destroy(project):
    build = get_devops_build(project)
    build.destroy(True)
```

### call the build

```bash
pyb apply
```
