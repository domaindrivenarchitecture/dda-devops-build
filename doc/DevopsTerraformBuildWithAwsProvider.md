# Aws Provider

## Input

| name                  | description                                                                                                                    | default        |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------ | -------------- |
| aws_access_key        | your aws access-key                                                                                                            |                |
| aws_secret_key        | your aws secret-key                                                                                                            |                |
| aws_as_backend        | you can use aws s3 as backend state storage                                                                                    | False          |
| aws_region            | in case of backend usage                                                                                                       | "eu-central-1" |
| aws_bucket            | in case of backend usage, the bucket your state is stored in. the url is S3://{aws_bucket}/{aws_bucket_key}/{aws_account_name} |                |
| aws_bucket_key        |                                                                                                                                | {module}       |
| aws_account_name      |                                                                                                                                | {stage}        |
| aws_bucket_kms_key_id | in case of backend usage, kms key for encrypting your state                                                                    |                |

### Credentials Mapping defaults
#### without a defined provider

```python
[
]
```

## Example Usage
### build.py

```python
from os import environ
from pybuilder.core import task, init
from ddadevops import *

name = 'my-project'
MODULE = 'my-module'
PROJECT_ROOT_PATH = '..'

@init
def initialize(project):
    project.build_depends_on("ddadevops>=4.0.0")

    input = {
        "credentials_mapping": [],
        "name": name,
        "module": MODULE,
        "name": name,
        "module": MODULE,
        "stage": environ["STAGE"],
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": ["TERRAFORM"],
        "mixin_types": [],
        "tf_provider_types": ["AWS"],
        "tf_use_workspace": False,
        "tf_terraform_semantic_version": "1.4.2",
        "aws_as_backend": True,
        "aws_bucket": "your-configuration",
        "aws_bucket_kms_key_id": "your-kms-key-id",
    }

    build = DevopsTerraformBuild(project, config)
    build.initialize_build_dir()


@task
def plan(project):
    build = get_devops_build(project)
    build.plan()


@task
def apply(project):
    build = get_devops_build(project)
    build.apply(True)


@task
def destroy(project):
    build = get_devops_build(project)
    build.destroy()


@task
def tf_import(project):
    build = get_devops_build(project)
    build.tf_import(
        "terraform.resource", "providers id"
    )
```

### call the build

```bash
pyb image plan apply destroy
```
