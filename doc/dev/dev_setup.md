

# For local development
```
python3 -m venv ~/.venv --upgrade  
source ~/.venv/bin/activate
pip3 install --upgrade -r dev_requirements.txt
pip3 install --upgrade -r requirements.txt
```

# For testing a dev version
```
pyb publish upload
pip3 install --upgrade ddadevops --pre
```
