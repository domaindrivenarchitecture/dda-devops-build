#  dda_devops_build
#  Copyright 2019 meissa GmbH.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from os import environ
from subprocess import run
from pybuilder.core import init, task, use_plugin, Author
from ddadevops import *


use_plugin("python.core")
use_plugin("copy_resources")
use_plugin("filter_resources")
# use_plugin("python.unittest")
# use_plugin("python.coverage")
use_plugin("python.distutils")

# use_plugin("python.install_dependencies")

default_task = "dev"

name = "ddadevops"
MODULE = "not-used"
PROJECT_ROOT_PATH = "."
version = "4.14.1-dev"
summary = "tools to support builds combining gopass, terraform, dda-pallet, aws & hetzner-cloud"
description = __doc__
authors = [Author("meissa GmbH", "buero@meissa-gmbh.de")]
url = "https://repo.prod.meissa.de/meissa/dda-devops-build"
requires_python = ">=3.10"  # CHECK IF NEW VERSION EXISTS
license = "Apache Software License"


@init
def initialize(project):
    # project.build_depends_on('mockito')
    # project.build_depends_on('unittest-xml-reporting')
    project.build_depends_on("ddadevops>=4.7.0")

    project.set_property("verbose", True)
    project.get_property("filter_resources_glob").append(
        "main/python/ddadevops/__init__.py"
    )
    project.set_property("dir_source_unittest_python", "src/test/python")

    project.set_property("copy_resources_target", "$dir_dist/ddadevops")
    project.get_property("copy_resources_glob").append("LICENSE")
    project.get_property("copy_resources_glob").append("src/main/resources/terraform/*")
    project.get_property("copy_resources_glob").append(
        "src/main/resources/docker/image/resources/*"
    )
    project.include_file("ddadevops", "LICENSE")
    project.include_file("ddadevops", "src/main/resources/terraform/*")
    project.include_file("ddadevops", "src/main/resources/docker/image/resources/*")

    # project.set_property('distutils_upload_sign', True)
    # project.set_property('distutils_upload_sign_identity', '')
    project.set_property("distutils_readme_description", True)
    project.set_property("distutils_description_overwrite", True)
    project.set_property(
        "distutils_classifiers",
        [
            "License :: OSI Approved :: Apache Software License",
            "Programming Language :: Python",
            "Programming Language :: Python :: 3",
            "Programming Language :: Python :: 3.8",
            "Programming Language :: Python :: 3.10",
            "Operating System :: POSIX :: Linux",
            "Operating System :: OS Independent",
            "Development Status :: 5 - Production/Stable",
            "Environment :: Console",
            "Intended Audience :: Developers",
            "License :: OSI Approved :: Apache Software License",
            "Topic :: Software Development :: Build Tools",
            "Topic :: Software Development :: Quality Assurance",
            "Topic :: Software Development :: Testing",
        ],
    )

    input = {
        "name": name,
        "module": MODULE,
        "stage": "notused",
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": [],
        "mixin_types": ["RELEASE"],
        "release_primary_build_file": "build.py",
        "release_secondary_build_files": [
            "infrastructure/python/build.py",
            "infrastructure/dind/build.py",
            "infrastructure/ddadevops/build.py",
            "infrastructure/clj-cljs/build.py",
            "infrastructure/clj/build.py",
            "infrastructure/kotlin/build.py",
        ],
        "release_artifacts": [],
        "release_artifact_server_url": "https://repo.prod.meissa.de",
        "release_organisation": "meissa",
        "release_repository_name": "dda-devops-build",
    }

    build = ReleaseMixin(project, input)
    build.initialize_build_dir()


@task
def test(project):
    run("pytest", check=True)


@task
def lint(project):
    run(
        "flake8 --max-line-length=120 --count --select=E9,F63,F7,F82 "
        + "--show-source --statistics src/main/python/ddadevops/",
        shell=True,
        check=True,
    )
    run(
        "flake8 --count --exit-zero --max-complexity=10 --max-line-length=127 "
        + '--per-file-ignores="__init__.py:F401" '
        + "--ignore=E722,W503 --statistics src/main/python/ddadevops/",
        shell=True,
        check=True,
    )
    run(
        "python -m mypy src/main/python/ddadevops/ --ignore-missing-imports "
        + "--disable-error-code=attr-defined --disable-error-code=union-attr",
        shell=True,
        check=True,
    )
    

@task
def patch(project):
    linttest(project, "PATCH")
    release(project)


@task
def minor(project):
    linttest(project, "MINOR")
    release(project)


@task
def major(project):
    linttest(project, "MAJOR")
    release(project)


@task
def dev(project):
    linttest(project, "NONE")


@task
def prepare(project):
    build = get_devops_build(project)
    build.prepare_release()


@task
def tag(project):
    build = get_devops_build(project)
    build.tag_bump_and_push_release()


@task
def publish_artifacts(project):
    build = get_devops_build(project)
    build.publish_artifacts()


def release(project):
    prepare(project)
    tag(project)


def linttest(project, release_type):
    build = get_devops_build(project)
    build.update_release_type(release_type)
    test(project)
    lint(project)
