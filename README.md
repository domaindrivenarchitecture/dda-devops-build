# dda-devops-build

[![Slack](https://img.shields.io/badge/chat-clojurians-green.svg?style=flat)](https://clojurians.slack.com/messages/#dda-pallet/) | [<img src="https://domaindrivenarchitecture.org/img/delta-chat.svg" width=20 alt="DeltaChat"> chat over e-mail](mailto:buero@meissa-gmbh.de?subject=community-chat) | [<img src="https://meissa.de/images/parts/contact/mastodon36_hue9b2464f10b18e134322af482b9c915e_5501_filter_14705073121015236177.png" width=20 alt="M"> meissa@social.meissa-gmbh.de](https://social.meissa-gmbh.de/@meissa) | [Blog](https://domaindrivenarchitecture.org) | [Website](https://meissa.de)


dda-devops-build integrates all the tools we use to work with clouds & provide some nice functions around.

Tools we support are

* terraform: for setting up the plain infrastructure around.
* docker: for creating images
* c4k: for generating kubernetes manifests
* provs: for setting up small single-node k3s clusters
* gopass: for credential management on devops computers
* cloud providers: hetzner, digitalocean, aws

In addition we provide a ReleaseMixin for release related tasks like tag / publish & version-bump

```mermaid
classDiagram
    class DevopsBuild {
        name()
        build_path()
        initialize_build_dir()
    }

    class DevopsTerraformBuild {
        initialize_build_dir()
        post_build()
        read_output_json()
        plan()
        plan_fail_on_diff()
        apply(auto_approve=False)
        refresh()
        destroy(auto_approve=False)
        tf_import(tf_import_name,tf_import_resource)
    }

    class DevopsImageBuild {
        initialize_build_dir()
        image()
        drun()
        dockerhub_login()
        dockerhub_publish()
        test()
    }

    class ReleaseMixin {
        prepare_release()
        tag_and_push_release()
    }
    
    class ProvsK3sBuild {
        def update_runtime_config(dns_record)
        write_provs_config()
        provs_apply(dry_run=False)
    }

    class C4kBuild {
        def update_runtime_config(dns_record)
        def write_c4k_config()
        def write_c4k_auth()
        c4k_apply(dry_run=False)
    }

    DevopsBuild <|-- DevopsImageBuild
    DevopsBuild <|-- DevopsTerraformBuild
    DevopsBuild <|-- ReleaseMixin
    DevopsBuild <|-- ProvsK3sBuild
    DevopsBuild <|-- C4kBuild

    link DevopsBuild "dda-devops-build/src/doc/DevopsBuild.md"
    link DevopsImageBuild "dda-devops-build/src/doc/DevopsImageBuild.md"
    link DevopsTerraformBuild "dda-devops-build/src/doc/DevopsTerraformBuild.md"
    link ReleaseMixin "dda-devops-build/src/doc/ReleaseMixin.md"
    link ProvsK3sBuild "dda-devops-build/src/doc/ProvsK3sBuild.md"
    link C4kBuild "dda-devops-build/src/doc/C4kBuild.md"

```

Principles we follow are:

* Seperate build artefacts from version controlled code
* Domain Driven Design - in order to stay sustainable

## Example Project

An example project which is using dda-devops-build can be found at: https://repo.prod.meissa.de/meissa/buildtest

## Installation

Ensure that yout python3 version is at least Python 3.10

```
sudo apt install python3-pip
pip3 install -r requirements.txt
export PATH=$PATH:~/.local/bin
```

## Example Project

An example project which is using dda-devops-build can be found at: https://repo.prod.meissa.de/meissa/buildtest

## Example Build

lets assume the following project structure

```
my-project
   | -> my-module
   |       | -> build.py
   |       | -> some-terraform.tf
   | -> an-other-module
   | -> target  (here will the build happen)
   |       | -> ...
```

```python
from pybuilder.core import task, init
from ddadevops import *

name = 'my-project'
MODULE = 'my-module'
PROJECT_ROOT_PATH = '..'


@init
def initialize(project):
    project.build_depends_on("ddadevops>=4.0.0-dev")

    config = {
        "credentials_mapping": [
            {
                "gopass_path": environ.get("DIGITALOCEAN_TOKEN_KEY_PATH", None),
                "name": "do_api_key",
            },
            {
                "gopass_path": environ.get("HETZNER_API_KEY_PATH", None),
                "name": "hetzner_api_key",
            },
        ],
        "name": name,
        "module": MODULE,
        "stage": environ["STAGE"],
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": ["TERRAFORM"],
        "mixin_types": [],
        "tf_provider_types": ["DIGITALOCEAN", "HETZNER"],
        "tf_use_workspace": False,
        "tf_terraform_semantic_version": "1.4.2",
        "do_as_backend": True,
        "do_bucket": "your-bucket",
    }

    build = DevopsTerraformBuild(project, config)
    build.initialize_build_dir()


@task
def plan(project):
    build = get_devops_build(project)
    build.plan()


@task
def apply(project):
    build = get_devops_build(project)
    build.apply(True)


@task
def destroy(project):
    build = get_devops_build(project)
    build.destroy(True)

```

## Snapshot & Release

```
pyb dev publish upload
pip3 install --upgrade ddadevops --pre

pyb [patch|minor|major]
pip3 install --upgrade ddadevops
```

## Reference

* [DevopsBuild](./doc/DevopsBuild.md)
* [DevopsImageBuild](./doc/DevopsImageBuild.md)
* [DevopsTerraformBuild](./doc/DevopsTerraformBuild.md)
  * [AwsProvider](doc/DevopsTerraformBuildWithAwsProvider.md)
  * [DigitaloceanProvider](doc/DevopsTerraformBuildWithDigitaloceanProvider.md)
  * [HetznerProvider](doc/DevopsTerraformBuildWithHetznerProvider.md)
* [ReleaseMixin](./doc/ReleaseMixin.md)
* [ProvsK3sBuild](doc/ProvsK3sBuild.md)
* [C4kBuild](doc/C4kBuild.md)


## Development & mirrors

Development happens at: https://repo.prod.meissa.de/meissa/dda-devops-build

Mirrors are:

* https://gitlab.com/domaindrivenarchitecture/dda-devops-build (issues and PR, CI)
* https://github.com/DomainDrivenArchitecture/dda-devops-build

For more details about our repository model see: https://repo.prod.meissa.de/meissa/federate-your-repos

## License

Copyright © 2021 meissa GmbH
Licensed under the [Apache License, Version 2.0](LICENSE) (the "License")
