#!/bin/sh

set -exo pipefail

function main() {
    {
        upgradeSystem
        
        apk add --no-cache build-base rust python3 python3-dev py3-pip py3-setuptools py3-wheel libffi-dev openssl-dev cargo bash git curl
        python3 -m pip install -U pip
        pip3 install pybuilder ddadevops deprecation dda-python-terraform boto3 pyyaml inflection \
                coverage flake8 flake8-polyfill mypy mypy-extensions pycodestyle pyflakes pylint pytest pytest-cov pytest-datafiles types-setuptools types-PyYAML
        
        cleanupDocker
        
    } > /dev/null
}

source /tmp/install_functions_alpine.sh
main
