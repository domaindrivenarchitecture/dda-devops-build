#! /usr/bin/env bb

(ns restic-management
  (:require
   [clojure.spec.alpha :as s]
   [clojure.java.io :as io]
   [clojure.edn :as edn]))

(s/def ::state string?)

(s/def ::backup-repository-state
  (s/keys :req-un [::state]))

(def state {:state ""})

(defn store-backup-repository-state [s]
  (spit "backup-repository-state.edn" s))

(defn read-backup-repository-state []
   (try
     (with-open [r (io/reader "backup-repository-state.edn")]
       (edn/read (java.io.PushbackReader. r)))
  
     (catch java.io.IOException e
       (printf "Couldn't open '%s': %s\n" "backup-repository-state.edn" (.getMessage e)))
     (catch RuntimeException e
       (printf "Error parsing edn file '%s': %s\n" "backup-repository-state.edn" (.getMessage e)))))

(defn read-secret [s]
  (slurp (str "/var/run/secrets/" s)))
   ;"/var/run/secrets/rotation-credential-secret/rotation-credential"))

;(println (read-backup-repository-state))

;(println (:state (read-backup-repository-state)))

;(println (s/valid? ::backup-repository-state (read-backup-repository-state)))

(println (read-secret "rotation-credential-secret/rotation-credential"))
(println (read-secret "backup-secrets/restic-password"))

(s/def ::new-password string?)
(s/def ::old-password string?)
(s/def ::password-state
  (s/keys :req-un [::new-password ::old-password]))

(defn rotate []
  (let [state {:new-password (read-secret "rotation-credential-secret/rotation-credential")
               :old-password (read-secret "backup-secrets/restic-password")}]
    (store-backup-repository-state (prn-str state))))
(rotate)
