backup_pg_role_path='pg-role'
backup_pg_database_path='pg-database'

function init-command() {
  restic -r ${RESTIC_REPOSITORY}/${backup_pg_role_path} -v init $@
}

function init-role-repo() {

  if [ -z ${CERTIFICATE_FILE} ];
  then
    init-command
  else
    init-command --cacert ${CERTIFICATE_FILE}
  fi
  
}

function init-database-command() {
  restic -r ${RESTIC_REPOSITORY}/${backup_pg_database_path} -v init $@
}

function init-database-repo() {

  if [ -z ${CERTIFICATE_FILE} ];
  then
    init-database-command 
  else
    init-database-command --cacert ${CERTIFICATE_FILE}
  fi
}

function drop-create-db() {
  psql -d template1 -h ${POSTGRES_SERVICE} -p ${POSTGRES_PORT} -U ${POSTGRES_USER} \
      --no-password -c "DROP DATABASE \"${POSTGRES_DB}\";"
  psql -d template1 -h ${POSTGRES_SERVICE} -p ${POSTGRES_PORT} -U ${POSTGRES_USER} \
      --no-password -c "CREATE DATABASE \"${POSTGRES_DB}\";"
}

function create-pg-pass() {
  local pg_host=${POSTGRES_HOST:-localhost}

  echo "${pg_host}:${POSTGRES_DB}:${POSTGRES_USER}:${POSTGRES_PASSWORD}" > /root/.pgpass
	echo "${POSTGRES_HOST}:template1:${POSTGRES_USER}:${POSTGRES_PASSWORD}" >> /root/.pgpass
	chmod 0600 /root/.pgpass
}

function roles-unlock-command() {
  restic -v -r ${RESTIC_REPOSITORY}/${backup_pg_role_path} unlock --cleanup-cache $@
}

function roles-forget-command() {
  restic -v -r ${RESTIC_REPOSITORY}/${backup_pg_role_path} forget --group-by '' --keep-last 1 --keep-daily ${RESTIC_DAYS_TO_KEEP} --keep-monthly ${RESTIC_MONTHS_TO_KEEP} --prune $@
}

function backup-roles() {
  local role_prefix="$1"; shift

  if [ -z ${CERTIFICATE_FILE} ];
  then
    roles-unlock-command
    pg_dumpall -h ${POSTGRES_SERVICE} -p ${POSTGRES_PORT} -U${POSTGRES_USER} --no-password --roles-only | \
      grep ${role_prefix} | restic -r ${RESTIC_REPOSITORY}/${backup_pg_role_path} backup --stdin
    roles-forget-command
  else
    roles-unlock-command --cacert ${CERTIFICATE_FILE}
    pg_dumpall -h ${POSTGRES_SERVICE} -p ${POSTGRES_PORT} -U${POSTGRES_USER} --no-password --roles-only | \
      grep ${role_prefix} | restic -r ${RESTIC_REPOSITORY}/${backup_pg_role_path} backup --stdin --cacert ${CERTIFICATE_FILE}
    roles-forget-command --cacert ${CERTIFICATE_FILE}
  fi
}

function db-unlock-command() {
  restic -v -r ${RESTIC_REPOSITORY}/${backup_pg_database_path} unlock --cleanup-cache $@
}

function db-forget-command() {
  restic -v -r ${RESTIC_REPOSITORY}/${backup_pg_database_path} forget --group-by '' --keep-last 1 --keep-daily ${RESTIC_DAYS_TO_KEEP} --keep-monthly ${RESTIC_MONTHS_TO_KEEP} --prune $@
}

function backup-db-dump() {

  if [ -z ${CERTIFICATE_FILE} ];
  then
    db-unlock-command
    pg_dump -d ${POSTGRES_DB} -h ${POSTGRES_SERVICE} -p ${POSTGRES_PORT} \
      -U ${POSTGRES_USER} --no-password --serializable-deferrable | \
      restic -r ${RESTIC_REPOSITORY}/${backup_pg_database_path} backup --stdin
    db-forget-command
  else
    db-unlock-command --cacert ${CERTIFICATE_FILE}
    pg_dump -d ${POSTGRES_DB} -h ${POSTGRES_SERVICE} -p ${POSTGRES_PORT} \
      -U ${POSTGRES_USER} --no-password --serializable-deferrable | \
      restic -r ${RESTIC_REPOSITORY}/${backup_pg_database_path} backup --stdin --cacert ${CERTIFICATE_FILE}
    db-forget-command --cacert ${CERTIFICATE_FILE}
  fi
}

function restore-roles() {
  local snapshot_id="${1:-latest}"; shift

  if [ -z ${CERTIFICATE_FILE} ];
  then
    roles-unlock-command
    restic -r ${RESTIC_REPOSITORY}/${backup_pg_role_path} dump ${snapshot_id} stdin | \
      psql -d template1 -h ${POSTGRES_SERVICE} -p ${POSTGRES_PORT} -U ${POSTGRES_USER} \
      --no-password
  else
    roles-unlock-command --cacert ${CERTIFICATE_FILE}
    restic -r ${RESTIC_REPOSITORY}/${backup_pg_role_path} dump ${snapshot_id} stdin  --cacert ${CERTIFICATE_FILE} | \
      psql -d template1 -h ${POSTGRES_SERVICE} -p ${POSTGRES_PORT} -U ${POSTGRES_USER} \
      --no-password
  fi
}

function restore-db() {
  local snapshot_id="${1:-latest}"; shift
  
  if [ -z ${CERTIFICATE_FILE} ];
  then
    db-unlock-command
    restic -r ${RESTIC_REPOSITORY}/${backup_pg_database_path} dump ${snapshot_id} stdin | \
      psql -d ${POSTGRES_DB} -h ${POSTGRES_SERVICE} -p ${POSTGRES_PORT} -U ${POSTGRES_USER} \
      --no-password
  else
    db-unlock-command --cacert ${CERTIFICATE_FILE}
    restic -r ${RESTIC_REPOSITORY}/${backup_pg_database_path} dump ${snapshot_id} stdin --cacert ${CERTIFICATE_FILE} | \
      psql -d ${POSTGRES_DB} -h ${POSTGRES_SERVICE} -p ${POSTGRES_PORT} -U ${POSTGRES_USER} \
      --no-password
  fi
}

function list-snapshot-roles() {
  if [ -z ${CERTIFICATE_FILE} ];
  then
    restic -r ${RESTIC_REPOSITORY}/${backup_pg_role_path} snapshots
  else
    restic -r ${RESTIC_REPOSITORY}/${backup_pg_database_path} snapshots --cacert ${CERTIFICATE_FILE}
  fi
}

function list-snapshot-db() {
  if [ -z ${CERTIFICATE_FILE} ];
  then
    restic -r ${RESTIC_REPOSITORY}/${backup_pg_database_path} snapshots
  else
    restic -r ${RESTIC_REPOSITORY}/${backup_pg_database_path} snapshots --cacert ${CERTIFICATE_FILE}
  fi
}