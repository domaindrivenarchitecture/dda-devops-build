backup_file_path='files'

function init-file-repo() {
  if [ -z ${CERTIFICATE_FILE} ];
  then
    restic -r ${RESTIC_REPOSITORY}/${backup_file_path} -v init
  else
    restic -r ${RESTIC_REPOSITORY}/${backup_file_path} -v init --cacert ${CERTIFICATE_FILE}
  fi
}

# First arg is the directory, second is optional for the path to a certificate file
function backup-directory() {
  local directory="$1"; shift

  if [ -z ${CERTIFICATE_FILE} ];
  then
    restic -v -r ${RESTIC_REPOSITORY}/${backup_file_path} unlock --cleanup-cache
    cd ${directory} && restic -v -r ${RESTIC_REPOSITORY}/${backup_file_path} backup .
    restic -v -r ${RESTIC_REPOSITORY}/${backup_file_path} forget --group-by '' --keep-last 1 --keep-daily ${RESTIC_DAYS_TO_KEEP} --keep-monthly ${RESTIC_MONTHS_TO_KEEP} --prune
  else
    restic -v -r ${RESTIC_REPOSITORY}/${backup_file_path} unlock --cleanup-cache --cacert ${CERTIFICATE_FILE}
    cd ${directory} && restic -v -r ${RESTIC_REPOSITORY}/${backup_file_path} backup . --cacert ${CERTIFICATE_FILE}
    restic -v -r ${RESTIC_REPOSITORY}/${backup_file_path} forget --group-by '' --keep-last 1 --keep-daily ${RESTIC_DAYS_TO_KEEP} --keep-monthly ${RESTIC_MONTHS_TO_KEEP} --prune --cacert ${CERTIFICATE_FILE}
  fi
}

# First arg is the directory, the remaining args are the sub-directories (relative to the first directory) to backup.
function backup-fs-from-directory() {
  local directory="$1"; shift
  
  if [ -z ${CERTIFICATE_FILE} ];
  then
    restic -v -r ${RESTIC_REPOSITORY}/${backup_file_path} unlock --cleanup-cache
    cd ${directory} && restic -v -r ${RESTIC_REPOSITORY}/${backup_file_path} backup $@
    restic -v -r ${RESTIC_REPOSITORY}/${backup_file_path} forget --group-by '' --keep-last 1 --keep-daily ${RESTIC_DAYS_TO_KEEP} --keep-monthly ${RESTIC_MONTHS_TO_KEEP} --prune
  else
    restic -v -r ${RESTIC_REPOSITORY}/${backup_file_path} unlock --cleanup-cache --cacert ${CERTIFICATE_FILE}
    cd ${directory} && restic -v -r ${RESTIC_REPOSITORY}/${backup_file_path} backup $@ --cacert ${CERTIFICATE_FILE}
    restic -v -r ${RESTIC_REPOSITORY}/${backup_file_path} forget --group-by '' --keep-last 1 --keep-daily ${RESTIC_DAYS_TO_KEEP} --keep-monthly ${RESTIC_MONTHS_TO_KEEP} --prune --cacert ${CERTIFICATE_FILE}
  fi
  
}

# Das tut so nicht!
function restore-directory() {
  local directory="$1"; shift
  local snapshot_id="${1:-latest}"; shift

  if [ -z ${CERTIFICATE_FILE} ];
  then
    restic -v -r ${RESTIC_REPOSITORY}/${backup_file_path} unlock --cleanup-cache
    rm -rf ${directory}*
    restic -v -r $RESTIC_REPOSITORY/${backup_file_path} restore ${snapshot_id} --target ${directory}
  else
    restic -v -r ${RESTIC_REPOSITORY}/${backup_file_path} unlock --cleanup-cache --cacert ${CERTIFICATE_FILE}
    rm -rf ${directory}*
    restic -v -r $RESTIC_REPOSITORY/${backup_file_path} restore ${snapshot_id} --target ${directory} --cacert ${CERTIFICATE_FILE}
  fi
  
}

function list-snapshot-files() {
  if [ -z ${CERTIFICATE_FILE} ];
  then
    restic -r ${RESTIC_REPOSITORY}/${backup_file_path} snapshots
  else
    restic -r ${RESTIC_REPOSITORY}/${backup_file_path} snapshots --cacert ${CERTIFICATE_FILE}
  fi
}
