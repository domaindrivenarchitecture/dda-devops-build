#!/usr/bin/env bb

(require '[babashka.tasks :as tasks]
         '[dda.backup.management :as mgm])

(defn restic-repo-init!
  []
  (spit "restic-pwd" "ThePassword")
  (mgm/init! {:password-file "restic-pwd"
              :restic-repository "restic-repo"}))

(defn restic-backup!
  []
  (tasks/shell "mkdir" "test-backup")
  (spit "test-backup/file" "I was here")
  (tasks/shell "restic" "backup" "--password-file" "restic-pwd" "--repo" "restic-repo" "test-backup"))

(defn restic-restore!
  []
  (tasks/shell "mkdir" "test-restore")
  (tasks/shell "restic" "restore" "--password-file" "restic-pwd" "--repo" "restic-repo" "--target" "test-restore" "latest")
  )


(restic-repo-init!)
(restic-backup!)
(restic-restore!)
