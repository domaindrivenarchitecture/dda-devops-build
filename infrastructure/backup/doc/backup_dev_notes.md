## Init Statemachine

### Inputs
1. `restic-password: ""`
2. `restic-password-to-rotate: ""`

### Manual init the restic repository for the first time

1. apply backup-and-restore pod:   
   `kubectl scale deployment backup-restore --replicas=1`
2. exec into pod and execute restore pod (press tab to get your exact pod name)   
   `kubectl exec -it backup-restore-... -- /usr/local/bin/init.sh`
3. remove backup-and-restore pod:   
   `kubectl scale deployment backup-restore --replicas=0`

### Password Rotation

1. apply backup-and-restore pod:   
   `kubectl scale deployment backup-restore --replicas=1`
2. add new password to restic repository   
    `restic key add ....`   
    => Trigger ::   
    field (1) credential current   
    filed (2) credential new
3. replace field (1) with (2) & clear (2)
4. remove old key - ???  
`restic remove ....`


```mermaid
stateDiagram-v2
    [*] --> init
    init --> backup_ready: trigger, restic-password !empty
    backup_ready --> new_password_added: restic-password !empty && restic-password-to-rotate !empty 
    new_password_added --> backup_ready: restic-password !empty && restic-password-to-rotate empty
```

### First Steps

1. Cloud Testserver hochfahren
2. Dort backup-restore deployment (leeres Secret mgl.?), neues Secret "rotation-credential-secret" als Daten
3. mounten von angelegtem Secret in Pod backup-restore
4. ba*bash*ka Skript in pod starten -> liest Secret ?leer
5. Micha cons. 

```mermaid
sequenceDiagram
    participant k8s
    participant e as entrypoint.sh
    participant rm as restic-management.clj

    k8s ->> e: cronjob calls
    e ->> rm: start-file
    rm ->> rm: rotate
    activate rm
        rm ->> rm: read-backup-repository-state (state)
        rm ->> rm: read-secret (backup-secret/restic-password,  rotation-credential-secret/rotation-credential)
        rm ->> rm: switch
        activate rm
        rm ->> rm: if init && restic-password != null
        activate rm
        rm ->> rm: init.sh
        rm ->> rm: state init -> backup-ready
        deactivate rm
        rm ->> rm: if backup-ready && rotation-credential != null
        activate rm
        rm ->> rm: add-new-password-to-restic-repository.sh
        rm ->> rm: state backup-ready -> new-password-added
        deactivate rm
        rm ->> rm: if new-password-added && rotation-credential == null
        activate rm
        rm ->> rm: remove-old-password-from-restic-repository.sh
        rm ->> rm: state new-password-added -> backup-ready
        deactivate rm
        deactivate rm

        rm ->> rm: store-repository-state (state)
    deactivate rm
```