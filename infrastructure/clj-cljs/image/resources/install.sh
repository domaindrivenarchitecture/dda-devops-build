#!/bin/bash
set -exo pipefail

function main() {
    {
    upgradeSystem

    mkdir -p /usr/share/man/man1
    apt-get -qqy install curl openjdk-17-jre-headless leiningen
    
    # shadow-cljs
    npm install -g npm
    npm install -g --save-dev shadow-cljs

    # download kubeconform & graalvm
    kubeconform_version="0.6.4"

    curl -SsLo /tmp/kubeconform-linux-amd64.tar.gz https://github.com/yannh/kubeconform/releases/download/v${kubeconform_version}/kubeconform-linux-amd64.tar.gz
    curl -SsLo /tmp/CHECKSUMS https://github.com/yannh/kubeconform/releases/download/v${kubeconform_version}/CHECKSUMS

    #install clojure
    curl -SsLo /tmp/clojure-install.sh https://github.com/clojure/brew-install/releases/latest/download/linux-install.sh
    bash /tmp/clojure-install.sh

    # checksum kubeconform
    checksum

    # install kubeconform
    tar -C /usr/local/bin -xf /tmp/kubeconform-linux-amd64.tar.gz --exclude=LICENSE

    #install pyb
    apt-get -qqy install python3 python3-pip git
    pip3 install pybuilder 'ddadevops>=4.7.0' deprecation dda-python-terraform boto3 pyyaml inflection --break-system-packages

    #check
    lein --help

    cleanupDocker
    } > /dev/null
}

function checksum() {
    awk '{print $1 "  /tmp/" $2}' /tmp/CHECKSUMS|sed -n '2p' > /tmp/kubeconform-checksum
    cat /tmp/kubeconform-checksum
    sha256sum -c --status /tmp/kubeconform-checksum
}

source /tmp/install_functions_debian.sh
DEBIAN_FRONTEND=noninteractive DEBCONF_NOWARNINGS=yes main