#!/bin/sh

set -exo pipefail

function main() {
    {
        upgradeSystem
        
        apk add --no-cache python3 py3-pip openssl-dev bash git
        pip3 install --break-system-packages pybuilder ddadevops deprecation dda-python-terraform boto3 pyyaml inflection
        
        cleanupDocker        
    } > /dev/null
}

source /tmp/install_functions_alpine.sh
main
