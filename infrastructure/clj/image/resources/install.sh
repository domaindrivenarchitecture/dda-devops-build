#!/bin/bash
set -exo pipefail

function main() {
    {
    upgradeSystem

    apt-get -qqy install curl git openjdk-17-jre-headless leiningen build-essential libz-dev zlib1g-dev


    # download kubeconform & graalvm
    kubeconform_version="0.6.4"
    graalvm_jdk_version="21.0.2"

    curl -SsLo /tmp/kubeconform-linux-amd64.tar.gz https://github.com/yannh/kubeconform/releases/download/v${kubeconform_version}/kubeconform-linux-amd64.tar.gz
    curl -SsLo /tmp/CHECKSUMS https://github.com/yannh/kubeconform/releases/download/v${kubeconform_version}/CHECKSUMS
    curl -SsLo /tmp/graalvm-community-jdk.tar.gz https://github.com/graalvm/graalvm-ce-builds/releases/download/jdk-${graalvm_jdk_version}/graalvm-community-jdk-${graalvm_jdk_version}_linux-x64_bin.tar.gz
    curl -SsLo /tmp/graalvm-checksum https://github.com/graalvm/graalvm-ce-builds/releases/download/jdk-${graalvm_jdk_version}/graalvm-community-jdk-${graalvm_jdk_version}_linux-x64_bin.tar.gz.sha256

    # checksum kubeconform & graalvm-jdk
    checksum

    # install kubeconform
    tar -C /usr/local/bin -xf /tmp/kubeconform-linux-amd64.tar.gz --exclude=LICENSE

    # install graalvm
    tar -C /usr/lib/jvm/ -xf /tmp/graalvm-community-jdk.tar.gz
    dirname_graalvm=$(ls /usr/lib/jvm/|grep -e graa)
    ln -s /usr/lib/jvm/$dirname_graalvm /usr/lib/jvm/graalvm
    ln -s /usr/lib/jvm/graalvm/bin/gu /usr/local/bin
    update-alternatives --install /usr/bin/java java /usr/lib/jvm/graalvm/bin/java 2
    ln -s /usr/lib/jvm/graalvm/bin/native-image /usr/local/bin

    #install clojure
    curl -SsLo /tmp/clojure-install.sh https://github.com/clojure/brew-install/releases/latest/download/linux-install.sh
    bash /tmp/clojure-install.sh

    #install pyb
    apt-get -qqy install python3 python3-pip
    pip3 install pybuilder 'ddadevops>=4.7.0' deprecation dda-python-terraform boto3 pyyaml inflection --break-system-packages 

    #check
    native-image --version
    lein -v

    cleanupDocker
    } > /dev/null
}

function checksum() {
    #kubeconform
    awk '{print $1 "  /tmp/" $2}' /tmp/CHECKSUMS|sed -n '2p' > /tmp/kubeconform-checksum
    sha256sum -c --status /tmp/kubeconform-checksum
    
    #graalvm
    echo "  /tmp/graalvm-community-jdk.tar.gz"|tee -a /tmp/graalvm-checksum
    sha256sum -c --status /tmp/graalvm-checksum
}

source /tmp/install_functions_debian.sh
DEBIAN_FRONTEND=noninteractive DEBCONF_NOWARNINGS=yes main