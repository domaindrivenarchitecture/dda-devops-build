#!/bin/bash
set -exo pipefail

function main() {
    {
    upgradeSystem

    apt-get -qqy install curl git kotlin gradle iputils-ping ssh python3 python3-pip

    pip3 install --break-system-packages pybuilder 'ddadevops>=4.7.0' deprecation dda-python-terraform boto3 pyyaml inflection 

    cleanupDocker
    } > /dev/null
}

source /tmp/install_functions_debian.sh
DEBIAN_FRONTEND=noninteractive DEBCONF_NOWARNINGS=yes main